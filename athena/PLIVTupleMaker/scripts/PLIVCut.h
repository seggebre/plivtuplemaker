//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Nov 28 00:57:07 2023 by ROOT version 6.28/04
// from TTree PLIVCut/tree
// found on file: pliv_ntuple_run3.root
//////////////////////////////////////////////////////////

#ifndef PLIVCut_h
#define PLIVCut_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TTree.h"

// Header file for the classes stored in the TTree if any.
#include <vector>
#include <iostream>

class PLIVCut {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Float_t         weight_mc;
   Float_t         weight_pileup;
   Float_t         weight_beamspot;
   Float_t         weight_leptonSF;
   Float_t         weight_trigger;
   Float_t         weight_bTagSF_DL1dv01_Continuous;
   Float_t         weight_bTagSF_DL1dv01_77;
   Float_t         weight_jvt;
   Float_t         weight_pileup_UP;
   Float_t         weight_pileup_DOWN;
   Float_t         weight_leptonSF_EL_SF_Reco_UP;
   Float_t         weight_leptonSF_EL_SF_Reco_DOWN;
   Float_t         weight_leptonSF_EL_SF_ID_UP;
   Float_t         weight_leptonSF_EL_SF_ID_DOWN;
   Float_t         weight_leptonSF_EL_SF_Isol_UP;
   Float_t         weight_leptonSF_EL_SF_Isol_DOWN;
   Float_t         weight_trigger_EL_SF_Trigger_UP;
   Float_t         weight_trigger_EL_SF_Trigger_DOWN;
   Float_t         weight_trigger_MU_SF_STAT_UP;
   Float_t         weight_trigger_MU_SF_STAT_DOWN;
   Float_t         weight_trigger_MU_SF_SYST_UP;
   Float_t         weight_trigger_MU_SF_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
   Float_t         weight_jvt_UP;
   Float_t         weight_jvt_DOWN;
   std::vector<float>   *weight_bTagSF_DL1dv01_Continuous_eigenvars_B_up;
   std::vector<float>   *weight_bTagSF_DL1dv01_Continuous_eigenvars_C_up;
   std::vector<float>   *weight_bTagSF_DL1dv01_Continuous_eigenvars_Light_up;
   std::vector<float>   *weight_bTagSF_DL1dv01_Continuous_eigenvars_B_down;
   std::vector<float>   *weight_bTagSF_DL1dv01_Continuous_eigenvars_C_down;
   std::vector<float>   *weight_bTagSF_DL1dv01_Continuous_eigenvars_Light_down;
   std::vector<float>   *weight_bTagSF_DL1dv01_77_eigenvars_B_up;
   std::vector<float>   *weight_bTagSF_DL1dv01_77_eigenvars_C_up;
   std::vector<float>   *weight_bTagSF_DL1dv01_77_eigenvars_Light_up;
   std::vector<float>   *weight_bTagSF_DL1dv01_77_eigenvars_B_down;
   std::vector<float>   *weight_bTagSF_DL1dv01_77_eigenvars_C_down;
   std::vector<float>   *weight_bTagSF_DL1dv01_77_eigenvars_Light_down;
   Float_t         weight_bTagSF_DL1dv01_77_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_DL1dv01_77_extrapolation_from_charm_down;
   ULong64_t       eventNumber;
   UInt_t          runNumber;
   UInt_t          randomRunNumber;
   UInt_t          mcChannelNumber;
   Float_t         mu;
   Float_t         mu_actual;
   UInt_t          backgroundFlags;
   UInt_t          hasBadMuon;
   std::vector<float>   *el_pt;
   std::vector<float>   *el_eta;
   std::vector<float>   *el_cl_eta;
   std::vector<float>   *el_phi;
   std::vector<float>   *el_e;
   std::vector<float>   *el_charge;
   std::vector<float>   *el_topoetcone20;
   std::vector<float>   *el_ptvarcone20;
   std::vector<char>    *el_CF;
   std::vector<float>   *el_d0sig;
   std::vector<float>   *el_delta_z0_sintheta;
   std::vector<int>     *el_true_type;
   std::vector<int>     *el_true_origin;
   std::vector<int>     *el_true_firstEgMotherTruthType;
   std::vector<int>     *el_true_firstEgMotherTruthOrigin;
   std::vector<int>     *el_true_firstEgMotherPdgId;
   std::vector<char>    *el_true_isPrompt;
   std::vector<char>    *el_true_isChargeFl;
   std::vector<float>   *mu_pt;
   std::vector<float>   *mu_eta;
   std::vector<float>   *mu_phi;
   std::vector<float>   *mu_e;
   std::vector<float>   *mu_charge;
   std::vector<float>   *mu_topoetcone20;
   std::vector<float>   *mu_ptvarcone30;
   std::vector<float>   *mu_d0sig;
   std::vector<float>   *mu_delta_z0_sintheta;
   std::vector<int>     *mu_true_type;
   std::vector<int>     *mu_true_origin;
   std::vector<char>    *mu_true_isPrompt;
   std::vector<float>   *tjet_pt;
   std::vector<float>   *tjet_eta;
   std::vector<float>   *tjet_phi;
   std::vector<float>   *tjet_e;
   std::vector<float>   *idTrack_d0;
   std::vector<float>   *idTrack_d0Sig;
   std::vector<float>   *idTrack_e;
   std::vector<float>   *idTrack_eta;
   std::vector<unsigned char> *idTrack_nPixelHits;
   std::vector<unsigned char> *idTrack_nPixelHoles;
   std::vector<unsigned char> *idTrack_nPixelSharedHits;
   std::vector<unsigned char> *idTrack_nSCTHits;
   std::vector<unsigned char> *idTrack_nSCTHoles;
   std::vector<unsigned char> *idTrack_nSCTSharedHits;
   std::vector<unsigned char> *idTrack_nSiHits;
   std::vector<unsigned char> *idTrack_nSiHoles;
   std::vector<float>   *idTrack_phi;
   std::vector<float>   *idTrack_pt;
   std::vector<float>   *idTrack_z0;
   std::vector<float>   *idTrack_z0Sig;
   std::vector<float>   *idTrack_z0Sin;
   std::vector<float>   *mu_calE;
   std::vector<float>   *mu_calERel;
   std::vector<float>   *mu_dRlj;
   std::vector<float>   *mu_energyLoss;
   std::vector<int>     *mu_hasTrackJet;
   std::vector<int>     *mu_hasTrackMuon;
   std::vector<unsigned int> *mu_iffClass;
   std::vector<float>   *mu_ptFrac;
   std::vector<float>   *mu_ptRaw;
   std::vector<float>   *mu_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500;
   std::vector<float>   *mu_ptvarcone30Rel;
   std::vector<float>   *mu_topoetcone30;
   std::vector<float>   *mu_topoetcone30Rel;
   std::vector<float>   *mu_topoetcone40;
   std::vector<float>   *mu_topoetcone40Rel;
   std::vector<float>   *trackMu_pt;
   std::vector<float>   *trackMu_eta;
   std::vector<float>   *trackMu_phi;
   std::vector<float>   *trackMu_e;
   std::vector<int>     *trackMu_hasTrackJet;
   std::vector<float>   *trackMu_d0;
   std::vector<float>   *trackMu_d0Sig;
   std::vector<float>   *trackMu_dRTrackJet;
   std::vector<unsigned char> *trackMu_nPixelHits;
   std::vector<unsigned char> *trackMu_nPixelHoles;
   std::vector<unsigned char> *trackMu_nPixelSharedHits;
   std::vector<unsigned char> *trackMu_nSCTHits;
   std::vector<unsigned char> *trackMu_nSCTHoles;
   std::vector<unsigned char> *trackMu_nSCTSharedHits;
   std::vector<unsigned char> *trackMu_nSiHits;
   std::vector<unsigned char> *trackMu_nSiHoles;
   std::vector<float>   *trackMu_ptRelTrackJet;
   std::vector<float>   *trackMu_z0;
   std::vector<float>   *trackMu_z0Sig;
   std::vector<float>   *trackMu_z0Sin;
   std::vector<float>   *trackMu_trackJetPt;
   std::vector<float>   *trackMu_trackJetEta;
   std::vector<float>   *trackMu_trackJetPhi;
   std::vector<float>   *trackMu_trackJetE;
   std::vector<float>   *el_calELarge;
   std::vector<float>   *el_calELargeRel;
   std::vector<float>   *el_dRlj;
   std::vector<unsigned int> *el_iffClass;
   std::vector<int>     *el_hasTrackElec;
   std::vector<float>   *el_ptFrac;
   std::vector<float>   *el_ptRel;
   std::vector<float>   *el_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt500;
   std::vector<float>   *el_ptvarcone30Rel;
   std::vector<float>   *el_topoetcone40;
   std::vector<float>   *el_topoetcone40Rel;
   std::vector<float>   *el_trackJetNTracks;
   std::vector<float>   *trackEl_pt;
   std::vector<float>   *trackEl_eta;
   std::vector<float>   *trackEl_phi;
   std::vector<float>   *trackEl_e;

   // List of branches
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_beamspot;   //!
   TBranch        *b_weight_leptonSF;   //!
   TBranch        *b_weight_trigger;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_Continuous;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_77;   //!
   TBranch        *b_weight_jvt;   //!
   TBranch        *b_weight_pileup_UP;   //!
   TBranch        *b_weight_pileup_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_DOWN;   //!
   TBranch        *b_weight_trigger_EL_SF_Trigger_UP;   //!
   TBranch        *b_weight_trigger_EL_SF_Trigger_DOWN;   //!
   TBranch        *b_weight_trigger_MU_SF_STAT_UP;   //!
   TBranch        *b_weight_trigger_MU_SF_STAT_DOWN;   //!
   TBranch        *b_weight_trigger_MU_SF_SYST_UP;   //!
   TBranch        *b_weight_trigger_MU_SF_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_jvt_UP;   //!
   TBranch        *b_weight_jvt_DOWN;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_Continuous_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_Continuous_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_Continuous_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_Continuous_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_Continuous_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_Continuous_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_77_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_77_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_77_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_77_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_77_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_77_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_77_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_DL1dv01_77_extrapolation_from_charm_down;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_randomRunNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_mu_actual;   //!
   TBranch        *b_backgroundFlags;   //!
   TBranch        *b_hasBadMuon;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_cl_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_topoetcone20;   //!
   TBranch        *b_el_ptvarcone20;   //!
   TBranch        *b_el_CF;   //!
   TBranch        *b_el_d0sig;   //!
   TBranch        *b_el_delta_z0_sintheta;   //!
   TBranch        *b_el_true_type;   //!
   TBranch        *b_el_true_origin;   //!
   TBranch        *b_el_true_firstEgMotherTruthType;   //!
   TBranch        *b_el_true_firstEgMotherTruthOrigin;   //!
   TBranch        *b_el_true_firstEgMotherPdgId;   //!
   TBranch        *b_el_true_isPrompt;   //!
   TBranch        *b_el_true_isChargeFl;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_topoetcone20;   //!
   TBranch        *b_mu_ptvarcone30;   //!
   TBranch        *b_mu_d0sig;   //!
   TBranch        *b_mu_delta_z0_sintheta;   //!
   TBranch        *b_mu_true_type;   //!
   TBranch        *b_mu_true_origin;   //!
   TBranch        *b_mu_true_isPrompt;   //!
   TBranch        *b_tjet_pt;   //!
   TBranch        *b_tjet_eta;   //!
   TBranch        *b_tjet_phi;   //!
   TBranch        *b_tjet_e;   //!
   TBranch        *b_idTrack_d0;   //!
   TBranch        *b_idTrack_d0Sig;   //!
   TBranch        *b_idTrack_e;   //!
   TBranch        *b_idTrack_eta;   //!
   TBranch        *b_idTrack_nPixelHits;   //!
   TBranch        *b_idTrack_nPixelHoles;   //!
   TBranch        *b_idTrack_nPixelSharedHits;   //!
   TBranch        *b_idTrack_nSCTHits;   //!
   TBranch        *b_idTrack_nSCTHoles;   //!
   TBranch        *b_idTrack_nSCTSharedHits;   //!
   TBranch        *b_idTrack_nSiHits;   //!
   TBranch        *b_idTrack_nSiHoles;   //!
   TBranch        *b_idTrack_phi;   //!
   TBranch        *b_idTrack_pt;   //!
   TBranch        *b_idTrack_z0;   //!
   TBranch        *b_idTrack_z0Sig;   //!
   TBranch        *b_idTrack_z0Sin;   //!
   TBranch        *b_mu_calE;   //!
   TBranch        *b_mu_calERel;   //!
   TBranch        *b_mu_dRlj;   //!
   TBranch        *b_mu_energyLoss;   //!
   TBranch        *b_mu_hasTrackJet;   //!
   TBranch        *b_mu_hasTrackMuon;   //!
   TBranch        *b_mu_iffClass;   //!
   TBranch        *b_mu_ptFrac;   //!
   TBranch        *b_mu_ptRaw;   //!
   TBranch        *b_mu_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500;   //!
   TBranch        *b_mu_ptvarcone30Rel;   //!
   TBranch        *b_mu_topoetcone30;   //!
   TBranch        *b_mu_topoetcone30Rel;   //!
   TBranch        *b_mu_topoetcone40;   //!
   TBranch        *b_mu_topoetcone40Rel;   //!
   TBranch        *b_trackMu_pt;   //!
   TBranch        *b_trackMu_eta;   //!
   TBranch        *b_trackMu_phi;   //!
   TBranch        *b_trackMu_e;   //!
   TBranch        *b_trackMu_hasTrackJet;   //!
   TBranch        *b_trackMu_d0;   //!
   TBranch        *b_trackMu_d0Sig;   //!
   TBranch        *b_trackMu_dRTrackJet;   //!
   TBranch        *b_trackMu_nPixelHits;   //!
   TBranch        *b_trackMu_nPixelHoles;   //!
   TBranch        *b_trackMu_nPixelSharedHits;   //!
   TBranch        *b_trackMu_nSCTHits;   //!
   TBranch        *b_trackMu_nSCTHoles;   //!
   TBranch        *b_trackMu_nSCTSharedHits;   //!
   TBranch        *b_trackMu_nSiHits;   //!
   TBranch        *b_trackMu_nSiHoles;   //!
   TBranch        *b_trackMu_ptRelTrackJet;   //!
   TBranch        *b_trackMu_z0;   //!
   TBranch        *b_trackMu_z0Sig;   //!
   TBranch        *b_trackMu_z0Sin;   //!
   TBranch        *b_trackMu_trackJetPt;   //!
   TBranch        *b_trackMu_trackJetEta;   //!
   TBranch        *b_trackMu_trackJetPhi;   //!
   TBranch        *b_trackMu_trackJetE;   //!
   TBranch        *b_el_calELarge;   //!
   TBranch        *b_el_calELargeRel;   //!
   TBranch        *b_el_dRlj;   //!
   TBranch        *b_el_iffClass;   //!
   TBranch        *b_el_hasTrackElec;   //!
   TBranch        *b_el_ptFrac;   //!
   TBranch        *b_el_ptRel;   //!
   TBranch        *b_el_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt500;   //!
   TBranch        *b_el_ptvarcone30Rel;   //!
   TBranch        *b_el_topoetcone40;   //!
   TBranch        *b_el_topoetcone40Rel;   //!
   TBranch        *b_el_trackJetNTracks;   //!
   TBranch        *b_trackEl_pt;   //!
   TBranch        *b_trackEl_eta;   //!
   TBranch        *b_trackEl_phi;   //!
   TBranch        *b_trackEl_e;   //!

   PLIVCut(TTree *tree=0);
   PLIVCut(TTree *tree, bool isMuon5);
   virtual ~PLIVCut();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    Cut(size_t i);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(std::string fileName);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);

   private:
   float muon_pt;
   float muon_ptRaw;
   float muon_eta;
   float muon_phi;
   float muon_e;
   float muon_ptvarcone30Rel;
   float muon_topoetcone30Rel;
   float muon_topoetcone40Rel;
   float muon_ptFrac;
   float muon_dRlj;
   float muon_calERel;
   int muon_iff;
   int muon_isPrompt;
   bool m_isMuon5;
   std::vector<float> track_dRtj;
   std::vector<float> track_ptFrac;
   std::vector<float> track_z0Sin;
   std::vector<float> track_d0Sig;
   std::vector<float> track_nPixelHits;
   std::vector<float> track_nSCTHits;
   TTree tNew;
};

#endif

#ifdef PLIVCut_cxx
PLIVCut::PLIVCut(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
//   if (tree == 0) {
//      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("pliv_ntuple_run3.root");
//      if (!f || !f->IsOpen()) {
//         f = new TFile("pliv_ntuple_run3.root");
//      }
//      f->GetObject("PLIVCut",tree);
//
   //}
   // TTree *tNew = new TTree("pliv_tree", "pliv_tree");
   m_isMuon5 = false;
   Init(tree);
}

PLIVCut::PLIVCut(TTree *tree, bool isMuon5) : fChain(0), m_isMuon5(isMuon5)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
//   if (tree == 0) {
//      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("pliv_ntuple_run3.root");
//      if (!f || !f->IsOpen()) {
//         f = new TFile("pliv_ntuple_run3.root");
//      }
//      f->GetObject("PLIVCut",tree);
//
   //}
   // TTree *tNew = new TTree("pliv_tree", "pliv_tree");
   Init(tree);
}

PLIVCut::~PLIVCut()
{
   if (!fChain)
      return;
   delete fChain->GetCurrentFile();
   //delete tNew;
}

Int_t PLIVCut::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t PLIVCut::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void PLIVCut::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   weight_bTagSF_DL1dv01_Continuous_eigenvars_B_up = 0;
   weight_bTagSF_DL1dv01_Continuous_eigenvars_C_up = 0;
   weight_bTagSF_DL1dv01_Continuous_eigenvars_Light_up = 0;
   weight_bTagSF_DL1dv01_Continuous_eigenvars_B_down = 0;
   weight_bTagSF_DL1dv01_Continuous_eigenvars_C_down = 0;
   weight_bTagSF_DL1dv01_Continuous_eigenvars_Light_down = 0;
   weight_bTagSF_DL1dv01_77_eigenvars_B_up = 0;
   weight_bTagSF_DL1dv01_77_eigenvars_C_up = 0;
   weight_bTagSF_DL1dv01_77_eigenvars_Light_up = 0;
   weight_bTagSF_DL1dv01_77_eigenvars_B_down = 0;
   weight_bTagSF_DL1dv01_77_eigenvars_C_down = 0;
   weight_bTagSF_DL1dv01_77_eigenvars_Light_down = 0;
   el_pt = 0;
   el_eta = 0;
   el_cl_eta = 0;
   el_phi = 0;
   el_e = 0;
   el_charge = 0;
   el_topoetcone20 = 0;
   el_ptvarcone20 = 0;
   el_CF = 0;
   el_d0sig = 0;
   el_delta_z0_sintheta = 0;
   el_true_type = 0;
   el_true_origin = 0;
   el_true_firstEgMotherTruthType = 0;
   el_true_firstEgMotherTruthOrigin = 0;
   el_true_firstEgMotherPdgId = 0;
   el_true_isPrompt = 0;
   el_true_isChargeFl = 0;
   mu_pt = 0;
   mu_eta = 0;
   mu_phi = 0;
   mu_e = 0;
   mu_charge = 0;
   mu_topoetcone20 = 0;
   mu_ptvarcone30 = 0;
   mu_d0sig = 0;
   mu_delta_z0_sintheta = 0;
   mu_true_type = 0;
   mu_true_origin = 0;
   mu_true_isPrompt = 0;
   tjet_pt = 0;
   tjet_eta = 0;
   tjet_phi = 0;
   tjet_e = 0;
   idTrack_d0 = 0;
   idTrack_d0Sig = 0;
   idTrack_e = 0;
   idTrack_eta = 0;
   idTrack_nPixelHits = 0;
   idTrack_nPixelHoles = 0;
   idTrack_nPixelSharedHits = 0;
   idTrack_nSCTHits = 0;
   idTrack_nSCTHoles = 0;
   idTrack_nSCTSharedHits = 0;
   idTrack_nSiHits = 0;
   idTrack_nSiHoles = 0;
   idTrack_phi = 0;
   idTrack_pt = 0;
   idTrack_z0 = 0;
   idTrack_z0Sig = 0;
   idTrack_z0Sin = 0;
   mu_calE = 0;
   mu_calERel = 0;
   mu_dRlj = 0;
   mu_energyLoss = 0;
   mu_hasTrackJet = 0;
   mu_hasTrackMuon = 0;
   mu_iffClass = 0;
   mu_ptFrac = 0;
   mu_ptRaw = 0;
   mu_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500 = 0;
   mu_ptvarcone30Rel = 0;
   mu_topoetcone30 = 0;
   mu_topoetcone30Rel = 0;
   mu_topoetcone40 = 0;
   mu_topoetcone40Rel = 0;
   trackMu_pt = 0;
   trackMu_eta = 0;
   trackMu_phi = 0;
   trackMu_e = 0;
   trackMu_hasTrackJet = 0;
   trackMu_d0 = 0;
   trackMu_d0Sig = 0;
   trackMu_dRTrackJet = 0;
   trackMu_nPixelHits = 0;
   trackMu_nPixelHoles = 0;
   trackMu_nPixelSharedHits = 0;
   trackMu_nSCTHits = 0;
   trackMu_nSCTHoles = 0;
   trackMu_nSCTSharedHits = 0;
   trackMu_nSiHits = 0;
   trackMu_nSiHoles = 0;
   trackMu_ptRelTrackJet = 0;
   trackMu_z0 = 0;
   trackMu_z0Sig = 0;
   trackMu_z0Sin = 0;
   trackMu_trackJetPt = 0;
   trackMu_trackJetEta = 0;
   trackMu_trackJetPhi = 0;
   trackMu_trackJetE = 0;
   el_calELarge = 0;
   el_calELargeRel = 0;
   el_dRlj = 0;
   el_iffClass = 0;
   el_hasTrackElec = 0;
   el_ptFrac = 0;
   el_ptRel = 0;
   el_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt500 = 0;
   el_ptvarcone30Rel = 0;
   el_topoetcone40 = 0;
   el_topoetcone40Rel = 0;
   el_trackJetNTracks = 0;
   trackEl_pt = 0;
   trackEl_eta = 0;
   trackEl_phi = 0;
   trackEl_e = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
   fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
   fChain->SetBranchAddress("weight_beamspot", &weight_beamspot, &b_weight_beamspot);
   fChain->SetBranchAddress("weight_leptonSF", &weight_leptonSF, &b_weight_leptonSF);
   fChain->SetBranchAddress("weight_trigger", &weight_trigger, &b_weight_trigger);
   fChain->SetBranchAddress("weight_bTagSF_DL1dv01_Continuous", &weight_bTagSF_DL1dv01_Continuous, &b_weight_bTagSF_DL1dv01_Continuous);
   fChain->SetBranchAddress("weight_bTagSF_DL1dv01_77", &weight_bTagSF_DL1dv01_77, &b_weight_bTagSF_DL1dv01_77);
   fChain->SetBranchAddress("weight_jvt", &weight_jvt, &b_weight_jvt);
   fChain->SetBranchAddress("weight_pileup_UP", &weight_pileup_UP, &b_weight_pileup_UP);
   fChain->SetBranchAddress("weight_pileup_DOWN", &weight_pileup_DOWN, &b_weight_pileup_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_UP", &weight_leptonSF_EL_SF_Reco_UP, &b_weight_leptonSF_EL_SF_Reco_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_DOWN", &weight_leptonSF_EL_SF_Reco_DOWN, &b_weight_leptonSF_EL_SF_Reco_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_UP", &weight_leptonSF_EL_SF_ID_UP, &b_weight_leptonSF_EL_SF_ID_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_DOWN", &weight_leptonSF_EL_SF_ID_DOWN, &b_weight_leptonSF_EL_SF_ID_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_UP", &weight_leptonSF_EL_SF_Isol_UP, &b_weight_leptonSF_EL_SF_Isol_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_DOWN", &weight_leptonSF_EL_SF_Isol_DOWN, &b_weight_leptonSF_EL_SF_Isol_DOWN);
   fChain->SetBranchAddress("weight_trigger_EL_SF_Trigger_UP", &weight_trigger_EL_SF_Trigger_UP, &b_weight_trigger_EL_SF_Trigger_UP);
   fChain->SetBranchAddress("weight_trigger_EL_SF_Trigger_DOWN", &weight_trigger_EL_SF_Trigger_DOWN, &b_weight_trigger_EL_SF_Trigger_DOWN);
   fChain->SetBranchAddress("weight_trigger_MU_SF_STAT_UP", &weight_trigger_MU_SF_STAT_UP, &b_weight_trigger_MU_SF_STAT_UP);
   fChain->SetBranchAddress("weight_trigger_MU_SF_STAT_DOWN", &weight_trigger_MU_SF_STAT_DOWN, &b_weight_trigger_MU_SF_STAT_DOWN);
   fChain->SetBranchAddress("weight_trigger_MU_SF_SYST_UP", &weight_trigger_MU_SF_SYST_UP, &b_weight_trigger_MU_SF_SYST_UP);
   fChain->SetBranchAddress("weight_trigger_MU_SF_SYST_DOWN", &weight_trigger_MU_SF_SYST_DOWN, &b_weight_trigger_MU_SF_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_UP", &weight_leptonSF_MU_SF_ID_STAT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_UP", &weight_leptonSF_MU_SF_ID_SYST_UP, &b_weight_leptonSF_MU_SF_ID_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_DOWN", &weight_leptonSF_MU_SF_ID_SYST_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_UP", &weight_leptonSF_MU_SF_Isol_STAT_UP, &b_weight_leptonSF_MU_SF_Isol_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_DOWN", &weight_leptonSF_MU_SF_Isol_STAT_DOWN, &b_weight_leptonSF_MU_SF_Isol_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_UP", &weight_leptonSF_MU_SF_TTVA_STAT_UP, &b_weight_leptonSF_MU_SF_TTVA_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_DOWN", &weight_leptonSF_MU_SF_TTVA_STAT_DOWN, &b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_UP", &weight_leptonSF_MU_SF_Isol_SYST_UP, &b_weight_leptonSF_MU_SF_Isol_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_DOWN", &weight_leptonSF_MU_SF_Isol_SYST_DOWN, &b_weight_leptonSF_MU_SF_Isol_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_UP", &weight_leptonSF_MU_SF_TTVA_SYST_UP, &b_weight_leptonSF_MU_SF_TTVA_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_DOWN", &weight_leptonSF_MU_SF_TTVA_SYST_DOWN, &b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN);
   fChain->SetBranchAddress("weight_jvt_UP", &weight_jvt_UP, &b_weight_jvt_UP);
   fChain->SetBranchAddress("weight_jvt_DOWN", &weight_jvt_DOWN, &b_weight_jvt_DOWN);
   fChain->SetBranchAddress("weight_bTagSF_DL1dv01_Continuous_eigenvars_B_up", &weight_bTagSF_DL1dv01_Continuous_eigenvars_B_up, &b_weight_bTagSF_DL1dv01_Continuous_eigenvars_B_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1dv01_Continuous_eigenvars_C_up", &weight_bTagSF_DL1dv01_Continuous_eigenvars_C_up, &b_weight_bTagSF_DL1dv01_Continuous_eigenvars_C_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1dv01_Continuous_eigenvars_Light_up", &weight_bTagSF_DL1dv01_Continuous_eigenvars_Light_up, &b_weight_bTagSF_DL1dv01_Continuous_eigenvars_Light_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1dv01_Continuous_eigenvars_B_down", &weight_bTagSF_DL1dv01_Continuous_eigenvars_B_down, &b_weight_bTagSF_DL1dv01_Continuous_eigenvars_B_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1dv01_Continuous_eigenvars_C_down", &weight_bTagSF_DL1dv01_Continuous_eigenvars_C_down, &b_weight_bTagSF_DL1dv01_Continuous_eigenvars_C_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1dv01_Continuous_eigenvars_Light_down", &weight_bTagSF_DL1dv01_Continuous_eigenvars_Light_down, &b_weight_bTagSF_DL1dv01_Continuous_eigenvars_Light_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1dv01_77_eigenvars_B_up", &weight_bTagSF_DL1dv01_77_eigenvars_B_up, &b_weight_bTagSF_DL1dv01_77_eigenvars_B_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1dv01_77_eigenvars_C_up", &weight_bTagSF_DL1dv01_77_eigenvars_C_up, &b_weight_bTagSF_DL1dv01_77_eigenvars_C_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1dv01_77_eigenvars_Light_up", &weight_bTagSF_DL1dv01_77_eigenvars_Light_up, &b_weight_bTagSF_DL1dv01_77_eigenvars_Light_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1dv01_77_eigenvars_B_down", &weight_bTagSF_DL1dv01_77_eigenvars_B_down, &b_weight_bTagSF_DL1dv01_77_eigenvars_B_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1dv01_77_eigenvars_C_down", &weight_bTagSF_DL1dv01_77_eigenvars_C_down, &b_weight_bTagSF_DL1dv01_77_eigenvars_C_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1dv01_77_eigenvars_Light_down", &weight_bTagSF_DL1dv01_77_eigenvars_Light_down, &b_weight_bTagSF_DL1dv01_77_eigenvars_Light_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1dv01_77_extrapolation_from_charm_up", &weight_bTagSF_DL1dv01_77_extrapolation_from_charm_up, &b_weight_bTagSF_DL1dv01_77_extrapolation_from_charm_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1dv01_77_extrapolation_from_charm_down", &weight_bTagSF_DL1dv01_77_extrapolation_from_charm_down, &b_weight_bTagSF_DL1dv01_77_extrapolation_from_charm_down);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber);
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("mu", &mu, &b_mu);
   fChain->SetBranchAddress("mu_actual", &mu_actual, &b_mu_actual);
   fChain->SetBranchAddress("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
   fChain->SetBranchAddress("hasBadMuon", &hasBadMuon, &b_hasBadMuon);
   fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_cl_eta", &el_cl_eta, &b_el_cl_eta);
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_e", &el_e, &b_el_e);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("el_topoetcone20", &el_topoetcone20, &b_el_topoetcone20);
   fChain->SetBranchAddress("el_ptvarcone20", &el_ptvarcone20, &b_el_ptvarcone20);
   fChain->SetBranchAddress("el_CF", &el_CF, &b_el_CF);
   fChain->SetBranchAddress("el_d0sig", &el_d0sig, &b_el_d0sig);
   fChain->SetBranchAddress("el_delta_z0_sintheta", &el_delta_z0_sintheta, &b_el_delta_z0_sintheta);
   fChain->SetBranchAddress("el_true_type", &el_true_type, &b_el_true_type);
   fChain->SetBranchAddress("el_true_origin", &el_true_origin, &b_el_true_origin);
   fChain->SetBranchAddress("el_true_firstEgMotherTruthType", &el_true_firstEgMotherTruthType, &b_el_true_firstEgMotherTruthType);
   fChain->SetBranchAddress("el_true_firstEgMotherTruthOrigin", &el_true_firstEgMotherTruthOrigin, &b_el_true_firstEgMotherTruthOrigin);
   fChain->SetBranchAddress("el_true_firstEgMotherPdgId", &el_true_firstEgMotherPdgId, &b_el_true_firstEgMotherPdgId);
   fChain->SetBranchAddress("el_true_isPrompt", &el_true_isPrompt, &b_el_true_isPrompt);
   fChain->SetBranchAddress("el_true_isChargeFl", &el_true_isChargeFl, &b_el_true_isChargeFl);
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("mu_topoetcone20", &mu_topoetcone20, &b_mu_topoetcone20);
   fChain->SetBranchAddress("mu_ptvarcone30", &mu_ptvarcone30, &b_mu_ptvarcone30);
   fChain->SetBranchAddress("mu_d0sig", &mu_d0sig, &b_mu_d0sig);
   fChain->SetBranchAddress("mu_delta_z0_sintheta", &mu_delta_z0_sintheta, &b_mu_delta_z0_sintheta);
   fChain->SetBranchAddress("mu_true_type", &mu_true_type, &b_mu_true_type);
   fChain->SetBranchAddress("mu_true_origin", &mu_true_origin, &b_mu_true_origin);
   fChain->SetBranchAddress("mu_true_isPrompt", &mu_true_isPrompt, &b_mu_true_isPrompt);
   fChain->SetBranchAddress("tjet_pt", &tjet_pt, &b_tjet_pt);
   fChain->SetBranchAddress("tjet_eta", &tjet_eta, &b_tjet_eta);
   fChain->SetBranchAddress("tjet_phi", &tjet_phi, &b_tjet_phi);
   fChain->SetBranchAddress("tjet_e", &tjet_e, &b_tjet_e);
   fChain->SetBranchAddress("idTrack_d0", &idTrack_d0, &b_idTrack_d0);
   fChain->SetBranchAddress("idTrack_d0Sig", &idTrack_d0Sig, &b_idTrack_d0Sig);
   fChain->SetBranchAddress("idTrack_e", &idTrack_e, &b_idTrack_e);
   fChain->SetBranchAddress("idTrack_eta", &idTrack_eta, &b_idTrack_eta);
   fChain->SetBranchAddress("idTrack_nPixelHits", &idTrack_nPixelHits, &b_idTrack_nPixelHits);
   fChain->SetBranchAddress("idTrack_nPixelHoles", &idTrack_nPixelHoles, &b_idTrack_nPixelHoles);
   fChain->SetBranchAddress("idTrack_nPixelSharedHits", &idTrack_nPixelSharedHits, &b_idTrack_nPixelSharedHits);
   fChain->SetBranchAddress("idTrack_nSCTHits", &idTrack_nSCTHits, &b_idTrack_nSCTHits);
   fChain->SetBranchAddress("idTrack_nSCTHoles", &idTrack_nSCTHoles, &b_idTrack_nSCTHoles);
   fChain->SetBranchAddress("idTrack_nSCTSharedHits", &idTrack_nSCTSharedHits, &b_idTrack_nSCTSharedHits);
   fChain->SetBranchAddress("idTrack_nSiHits", &idTrack_nSiHits, &b_idTrack_nSiHits);
   fChain->SetBranchAddress("idTrack_nSiHoles", &idTrack_nSiHoles, &b_idTrack_nSiHoles);
   fChain->SetBranchAddress("idTrack_phi", &idTrack_phi, &b_idTrack_phi);
   fChain->SetBranchAddress("idTrack_pt", &idTrack_pt, &b_idTrack_pt);
   fChain->SetBranchAddress("idTrack_z0", &idTrack_z0, &b_idTrack_z0);
   fChain->SetBranchAddress("idTrack_z0Sig", &idTrack_z0Sig, &b_idTrack_z0Sig);
   fChain->SetBranchAddress("idTrack_z0Sin", &idTrack_z0Sin, &b_idTrack_z0Sin);
   fChain->SetBranchAddress("mu_calE", &mu_calE, &b_mu_calE);
   fChain->SetBranchAddress("mu_calERel", &mu_calERel, &b_mu_calERel);
   fChain->SetBranchAddress("mu_dRlj", &mu_dRlj, &b_mu_dRlj);
   fChain->SetBranchAddress("mu_energyLoss", &mu_energyLoss, &b_mu_energyLoss);
   fChain->SetBranchAddress("mu_hasTrackJet", &mu_hasTrackJet, &b_mu_hasTrackJet);
   fChain->SetBranchAddress("mu_hasTrackMuon", &mu_hasTrackMuon, &b_mu_hasTrackMuon);
   fChain->SetBranchAddress("mu_iffClass", &mu_iffClass, &b_mu_iffClass);
   fChain->SetBranchAddress("mu_ptFrac", &mu_ptFrac, &b_mu_ptFrac);
   fChain->SetBranchAddress("mu_ptRaw", &mu_ptRaw, &b_mu_ptRaw);
   fChain->SetBranchAddress("mu_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500", &mu_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500, &b_mu_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500);
   fChain->SetBranchAddress("mu_ptvarcone30Rel", &mu_ptvarcone30Rel, &b_mu_ptvarcone30Rel);
   fChain->SetBranchAddress("mu_topoetcone30", &mu_topoetcone30, &b_mu_topoetcone30);
   fChain->SetBranchAddress("mu_topoetcone30Rel", &mu_topoetcone30Rel, &b_mu_topoetcone30Rel);
   fChain->SetBranchAddress("mu_topoetcone40", &mu_topoetcone40, &b_mu_topoetcone40);
   fChain->SetBranchAddress("mu_topoetcone40Rel", &mu_topoetcone40Rel, &b_mu_topoetcone40Rel);
   fChain->SetBranchAddress("trackMu_pt", &trackMu_pt, &b_trackMu_pt);
   fChain->SetBranchAddress("trackMu_eta", &trackMu_eta, &b_trackMu_eta);
   fChain->SetBranchAddress("trackMu_phi", &trackMu_phi, &b_trackMu_phi);
   fChain->SetBranchAddress("trackMu_e", &trackMu_e, &b_trackMu_e);
   fChain->SetBranchAddress("trackMu_hasTrackJet", &trackMu_hasTrackJet, &b_trackMu_hasTrackJet);
   fChain->SetBranchAddress("trackMu_d0", &trackMu_d0, &b_trackMu_d0);
   fChain->SetBranchAddress("trackMu_d0Sig", &trackMu_d0Sig, &b_trackMu_d0Sig);
   fChain->SetBranchAddress("trackMu_dRTrackJet", &trackMu_dRTrackJet, &b_trackMu_dRTrackJet);
   fChain->SetBranchAddress("trackMu_nPixelHits", &trackMu_nPixelHits, &b_trackMu_nPixelHits);
   fChain->SetBranchAddress("trackMu_nPixelHoles", &trackMu_nPixelHoles, &b_trackMu_nPixelHoles);
   fChain->SetBranchAddress("trackMu_nPixelSharedHits", &trackMu_nPixelSharedHits, &b_trackMu_nPixelSharedHits);
   fChain->SetBranchAddress("trackMu_nSCTHits", &trackMu_nSCTHits, &b_trackMu_nSCTHits);
   fChain->SetBranchAddress("trackMu_nSCTHoles", &trackMu_nSCTHoles, &b_trackMu_nSCTHoles);
   fChain->SetBranchAddress("trackMu_nSCTSharedHits", &trackMu_nSCTSharedHits, &b_trackMu_nSCTSharedHits);
   fChain->SetBranchAddress("trackMu_nSiHits", &trackMu_nSiHits, &b_trackMu_nSiHits);
   fChain->SetBranchAddress("trackMu_nSiHoles", &trackMu_nSiHoles, &b_trackMu_nSiHoles);
   fChain->SetBranchAddress("trackMu_ptRelTrackJet", &trackMu_ptRelTrackJet, &b_trackMu_ptRelTrackJet);
   fChain->SetBranchAddress("trackMu_z0", &trackMu_z0, &b_trackMu_z0);
   fChain->SetBranchAddress("trackMu_z0Sig", &trackMu_z0Sig, &b_trackMu_z0Sig);
   fChain->SetBranchAddress("trackMu_z0Sin", &trackMu_z0Sin, &b_trackMu_z0Sin);
   fChain->SetBranchAddress("trackMu_trackJetPt", &trackMu_trackJetPt, &b_trackMu_trackJetPt);
   fChain->SetBranchAddress("trackMu_trackJetEta", &trackMu_trackJetEta, &b_trackMu_trackJetEta);
   fChain->SetBranchAddress("trackMu_trackJetPhi", &trackMu_trackJetPhi, &b_trackMu_trackJetPhi);
   fChain->SetBranchAddress("trackMu_trackJetE", &trackMu_trackJetE, &b_trackMu_trackJetE);
   fChain->SetBranchAddress("el_calELarge", &el_calELarge, &b_el_calELarge);
   fChain->SetBranchAddress("el_calELargeRel", &el_calELargeRel, &b_el_calELargeRel);
   fChain->SetBranchAddress("el_dRlj", &el_dRlj, &b_el_dRlj);
   fChain->SetBranchAddress("el_iffClass", &el_iffClass, &b_el_iffClass);
   fChain->SetBranchAddress("el_hasTrackElec", &el_hasTrackElec, &b_el_hasTrackElec);
   fChain->SetBranchAddress("el_ptFrac", &el_ptFrac, &b_el_ptFrac);
   fChain->SetBranchAddress("el_ptRel", &el_ptRel, &b_el_ptRel);
   fChain->SetBranchAddress("el_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt500", &el_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt500, &b_el_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt500);
   fChain->SetBranchAddress("el_ptvarcone30Rel", &el_ptvarcone30Rel, &b_el_ptvarcone30Rel);
   fChain->SetBranchAddress("el_topoetcone40", &el_topoetcone40, &b_el_topoetcone40);
   fChain->SetBranchAddress("el_topoetcone40Rel", &el_topoetcone40Rel, &b_el_topoetcone40Rel);
   fChain->SetBranchAddress("el_trackJetNTracks", &el_trackJetNTracks, &b_el_trackJetNTracks);
   fChain->SetBranchAddress("trackEl_pt", &trackEl_pt, &b_trackEl_pt);
   fChain->SetBranchAddress("trackEl_eta", &trackEl_eta, &b_trackEl_eta);
   fChain->SetBranchAddress("trackEl_phi", &trackEl_phi, &b_trackEl_phi);
   fChain->SetBranchAddress("trackEl_e", &trackEl_e, &b_trackEl_e);
   Notify();

   // Set new TTree
   tNew.Branch("muon_pt", &muon_pt, "muon_pt/F");
   tNew.Branch("muon_ptRaw", &muon_ptRaw, "muon_ptRaw/F");
   tNew.Branch("muon_eta", &muon_eta, "muon_eta/F");
   tNew.Branch("muon_phi", &muon_phi, "muon_phi/F");
   tNew.Branch("muon_e", &muon_e, "muon_phi/F");
   tNew.Branch("muon_ptvarcone30Rel", &muon_ptvarcone30Rel, "muon_ptvarcone30Rel/F");
   tNew.Branch("muon_topoetcone30Rel", &muon_topoetcone30Rel, "muon_topoetcone30Rel/F");
   tNew.Branch("muon_topoetcone40Rel", &muon_topoetcone40Rel, "muon_topoetcone40Rel/F");
   tNew.Branch("muon_ptFrac", &muon_ptFrac, "muon_ptFrac/F");
   tNew.Branch("muon_dRlj", &muon_dRlj, "muon_dRlj/F");
   tNew.Branch("muon_calERel", &muon_calERel, "muon_calERel/F");
   tNew.Branch("muon_iff", &muon_iff, "muon_iff/I");
   tNew.Branch("muon_isPrompt", &muon_isPrompt, "muon_isPrompt/I");
   tNew.Branch("track_dRtj", &track_dRtj);
   tNew.Branch("track_ptFrac", &track_ptFrac);
   tNew.Branch("track_z0Sin", &track_z0Sin);
   tNew.Branch("track_d0Sig", &track_d0Sig);
   tNew.Branch("track_nPixelHits", &track_nPixelHits);
   tNew.Branch("track_nSCTHits", &track_nSCTHits);
}

Bool_t PLIVCut::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void PLIVCut::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}

#endif // #ifdef PLIVCut_cxx
