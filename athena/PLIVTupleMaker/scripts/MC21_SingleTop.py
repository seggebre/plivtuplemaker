# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

import TopExamples.grid

TopExamples.grid.Add("single_top_mc21").datasets = [
    'mc21_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.deriv.DAOD_PHYS.e8453_e8455_s3873_s3874_r13829_r13831_p5855',
    ]