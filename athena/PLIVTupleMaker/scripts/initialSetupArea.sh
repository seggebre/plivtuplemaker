ASETUP="asetup AnalysisBase,24.2.37,here"
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh

lsetup git

mkdir build run
if [ -f "source/CMakeLists.txt" ]; then
	echo "CMake TXT exists!"
else
	cp source/PLIVNtupleMaker/share/GeneralCMakeLists.txt source/CMakeLists.txt
fi
cd build
eval $ASETUP
cd ..

echo "setupATLAS --quiet" > setup.sh
echo "lsetup git" >> setup.sh
echo "cd build/" >> setup.sh
echo "asetup --restore" >> setup.sh
echo "cd ../build" >> setup.sh
echo "if ls */setup.sh 1> /dev/null 2>&1; then" >> setup.sh
echo "  source */setup.sh" >> setup.sh
echo "fi" >> setup.sh
echo "cd .." >> setup.sh
echo "#lsetup panda" >> setup.sh
echo "#lsetup pyami" >> setup.sh
echo "#voms-proxy-init --voms atlas" >> setup.sh

echo "cd build/" > full_compile.sh
echo "cmake ../source" >> full_compile.sh
echo "cmake --build ./" >> full_compile.sh
echo "source */setup.sh" >> full_compile.sh
echo "cd .." >> full_compile.sh

echo "cd build/" > quick_compile.sh
echo "cmake --build ./" >> quick_compile.sh
echo "source */setup.sh" >> quick_compile.sh
echo "cd .." >> quick_compile.sh