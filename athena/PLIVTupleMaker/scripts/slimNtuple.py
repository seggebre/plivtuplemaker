import ROOT
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("input", help="input file path of the ntuple")
parser.add_argument("output", help="output file path of the ntuple")
args = parser.parse_args()

ROOT.gROOT.SetBatch(True)
#ROOT.gSystem.Load("PLIVCut_C.so")
ROOT.gROOT.LoadMacro("PLIVCut.C")

# fIn = ROOT.TFile()
# fIn.Open(args.input)
#with ROOT.TFile.Open(args.input, "read") as fIn:
#	t = fIn.Get("nominal")
#	t.SetDirectory(ROOT.nullptr)
fIn = ROOT.TFile(args.input)
t = fIn.Get("nominal")
isMuon5 = False
for entry in t:
	if entry.mcChannelNumber == 601589:
		isMuon5 = True
	break
if isMuon5:
	tProcess = ROOT.PLIVCut(t, True)
else:
	tProcess = ROOT.PLIVCut(t)
tProcess.Loop(args.output)