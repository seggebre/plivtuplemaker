#include "TopEvent/EventTools.h"
#include "TopConfiguration/TopConfig.h"

#include "TopObjectSelectionTools/TopObjectSelection.h"
#include "TopObjectSelectionTools/ElectronLikelihood.h"
#include "TopObjectSelectionTools/IsolationTools.h"
#include "TopObjectSelectionTools/Muon.h"
#include "TopObjectSelectionTools/SoftMuon.h"
#include "TopObjectSelectionTools/AntiMuon.h"
#include "TopObjectSelectionTools/Tau.h"
#include "TopObjectSelectionTools/Jet.h"
#include "TopObjectSelectionTools/TrackJet.h"
#include "TopObjectSelectionTools/JetGhostTrackSelection.h"
#include "TopObjectSelectionTools/TrackSelection.h"
#include "TopObjectSelectionTools/OverlapRemovalASG.h"
#include "TopConfiguration/ConfigurationSettings.h"

#include "PLIVTupleMaker/PLIVObjectLoader.h"
#include "PLIVTupleMaker/DummyOverlapRemoval.h"

#include "TopAnalysis/MsgCategory.h"
using namespace TopAnalysis;

namespace top{
  top::TopObjectSelection* PLIVObjectLoader::init(std::shared_ptr<top::TopConfig> topConfig){

      top::TopObjectSelection* objectSelection = new top::TopObjectSelection(topConfig->objectSelectionName());
      top::check(objectSelection->setProperty("config" , topConfig), "Failed to setProperty for top::TopObjectSelection");
      top::check(objectSelection->initialize(), "Failed to initialize top::TopObjectSelection");

      //Electron Configuration
      if (topConfig->useElectrons()) {
        if (topConfig->electronID().find("LH") != std::string::npos &&
                  topConfig->electronIDLoose().find("LH") != std::string::npos) {
          //user wants likelihood electrons
          objectSelection->electronSelection(new top::ElectronLikelihood(topConfig->electronPtcut(),
                                                                        topConfig->electronVetoLArCrack(),
                                                                        "LooseLH",
                                                                        "LooseLH",
                                                                        new top::StandardIsolation(
                                                                          "None",
                                                                          "None"),
                                                                        topConfig->applyTTVACut(),
                                                                        topConfig->useElectronChargeIDSelection()
                                                                        ));
        } else {
          ATH_MSG_ERROR("Not sure it makes sense to use a mix of LH and cut-based electrons for the tight/loose definitions\n"
            << "Tight electron definition is " << topConfig->electronID() << "\n"
            << "Loose electron definition is " << topConfig->electronIDLoose() << "\n"
            << "If it does make sense, feel free to fix this");
          throw std::runtime_error("Mixing LH and cut-based electron definitions for tight/loose");
        }
      }


      //Muon Configuration

      objectSelection->muonSelection(new top::Muon(topConfig->muonPtcut(), new top::StandardIsolation(topConfig->muonIsolation(), topConfig->muonIsolationLoose()), topConfig->applyTTVACut()) );

      //Soft Muon Configuration

      if(topConfig->useSoftMuons()){
        objectSelection->softmuonSelection(new top::SoftMuon(topConfig->softmuonPtcut()));
      }

      //Small R-Jet Configuration

      objectSelection->jetSelection(new top::Jet(topConfig->jetPtcut(), topConfig->jetEtacut()));

      //Large R-Jet Configuration

      if (topConfig->useLargeRJets()) {// not doing JVT cut for large-R jets
          objectSelection->largeJetSelection(new top::Jet(topConfig->largeRJetPtcut(), topConfig->largeRJetEtacut(), topConfig->largeRJetMasscut(), false));
      }

      //Track Jet Configuration

      if (topConfig->useTrackJets()) {
        objectSelection->trackJetSelection(new top::TrackJet(topConfig->trackJetPtcut(),
                                                              topConfig->trackJetEtacut()));
      }

      //Ghost Track Jets Configuration

      if (topConfig->useJetGhostTrack()) {
        objectSelection->jetGhostTrackSelection(new top::JetGhostTrackSelection(topConfig->ghostTrackspT(),
                          2.5,topConfig->ghostTracksVertexAssociation(),topConfig->jetPtGhostTracks(), 2.5));
      }

      //Track Configuration

      if (topConfig->useTracks()) {
        objectSelection->trackSelection(new top::TrackSelection(topConfig->trackPtcut(), topConfig->trackEtacut()));
      }

      //Overlap Removal
      
      objectSelection->overlapRemovalPostSelection(new DummyOverlapRemoval());

      return objectSelection;
  }
}