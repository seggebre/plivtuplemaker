#include "PLIVTupleMaker/DecorateMuons.h"
#include "PLIVTupleMaker/PLIVUtils.h"

#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h"
#include "TopEventSelectionTools/TreeManager.h"
#include "AsgTools/AnaToolHandle.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

#include "TMath.h"

#include <vector>
#include <limits>

namespace top{
  DecorateMuons::DecorateMuons() :
    m_maxLepTrackJetDr(1.0),
    m_maxDeltaZ0Sin(0.5),
    m_maxTopoEtCone30Rel(0.3)
  {
  }

  void DecorateMuons::ProcessEvent(const top::Event& event, ToolHandle<CP::IClassificationTool>& iffTool){
    
    m_mu_calE.clear();
    m_mu_calERel.clear();
    m_mu_dRlj.clear();
    m_mu_dRljAll.clear();
    m_mu_EnergyLoss.clear();
    m_mu_hasTrackJet.clear();
    m_mu_hasTrackMuon.clear();
    m_mu_iffClass.clear();
    m_mu_ptFrac.clear();
    m_mu_ptRaw.clear();
    m_mu_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500.clear();
    m_mu_ptvarcone30Rel.clear();
    m_mu_topoetcone30.clear();
    m_mu_topoetcone30Rel.clear();
    m_mu_topoetcone40.clear();
    m_mu_topoetcone40Rel.clear();

    m_trackMu_pt.clear();
    m_trackMu_eta.clear();
    m_trackMu_phi.clear();
    m_trackMu_e.clear();
    m_trackMu_hasTrackJet.clear();
    m_trackMu_d0.clear();
    m_trackMu_d0Sig.clear();
    m_trackMu_dRTrackJet.clear();
    m_trackMu_ptRelTrackJet.clear();
    m_trackMu_nPixelHits.clear();
    m_trackMu_nPixelHoles.clear();
    m_trackMu_nPixelSharedHits.clear();
    m_trackMu_nSCTHits.clear();
    m_trackMu_nSCTHoles.clear();
    m_trackMu_nSCTSharedHits.clear();
    m_trackMu_nSiHits.clear();
    m_trackMu_nSiHoles.clear();
    m_trackMu_nSiSharedHits.clear();
    m_trackMu_z0.clear();
    m_trackMu_z0Sin.clear();
    m_trackMu_z0Sig.clear();

    m_trackMuTrackJet_pt.clear();
    m_trackMuTrackJet_eta.clear();
    m_trackMuTrackJet_phi.clear();
    m_trackMuTrackJet_e.clear();

    static SG::AuxElement::ConstAccessor<float> acc_calorimeterE("calE");
    static SG::AuxElement::ConstAccessor<float> acc_energyloss("EnergyLoss");

    const xAOD::Vertex *primaryVertex = PLIV::findPrimaryVertex(event.m_primaryVertices);

    for (auto muon : event.m_muons){
      float calE = -99.;
      float calERel = -99.;
      float dRlj = -99.;
      float dRljAll = -99.;
      int hasTrackMuon = 0;
      int hasTrackJet = 0;
      float energyLoss = -99.;
      unsigned int iffClass = 0;
      float ptFrac = -99.;
      float ptRaw = -99.;
      float ptvarcone30 = -99.;
      float ptvarcone30Rel = -99.;
      float topoetcone30 = -99.;
      float topoetcone30Rel = -99.;
      float topoetcone40 = -99.;
      float topoetcone40Rel = -99.;
      int trackMuonHasTrackJet = 0;
      float trackJetPt = -99.;
      float trackJetEta = -99.;
      float trackJetPhi = -99.;
      float trackJetE = -99.;

      ptRaw = muon->primaryTrackParticle()->pt();
      top::check(iffTool->classify(*muon, iffClass), "Fail to get IFF class of this muon");
      if (muon->isAvailable<float>("ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500")){
        ptvarcone30 = muon->auxdata<float>("ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500");
        ptvarcone30Rel = ptvarcone30 / ptRaw;
        }
      if (muon->isolation(topoetcone30, xAOD::Iso::topoetcone30))
      {
        topoetcone30Rel = topoetcone30 / ptRaw;
      }
      else
      {
        ANA_MSG_WARNING("Can not retrieve topoetcone30");
        continue;
      }
      if (muon->isolation(topoetcone40, xAOD::Iso::topoetcone40))
      {
        topoetcone40Rel = topoetcone40 / ptRaw;
      }
      else
      {
        ANA_MSG_WARNING("Can not retrieve topoetcone40");
        continue;
      }

      if (topoetcone30Rel > m_maxTopoEtCone30Rel)
        continue;

      auto closestTrackJet = top::PLIV::findClosestJet(*muon, event.m_trackJets, m_maxLepTrackJetDr);
      if (closestTrackJet){
        hasTrackJet = 1;
        ptFrac = muon->primaryTrackParticle()->pt() / closestTrackJet->pt();
        dRlj = closestTrackJet->p4().DeltaR(muon->p4());
      }
      auto closestTrackJetAll = top::PLIV::findClosestJet(*muon, event.m_trackJets, std::numeric_limits<float>::max());
      if (closestTrackJetAll){
        dRljAll = closestTrackJetAll->p4().DeltaR(muon->p4());
      }
      //if (closestTrackJetLarge){
      //      dRljAll = closestTrackJetLarge->p4().DeltaR(muon->p4());
      //}
      //else{
      //  ANA_MSG_WARNING("No track jet near the muon!");
      //}
      
      // read cluster information
      /*if(muon->clusterLink().isValid()) 
      {
        const xAOD::CaloCluster *cluster = *(muon->clusterLink());
        calE = cluster->calE();
      }
      else
      {
        ANA_MSG_DEBUG("Invalid calo cluster link of the muon");
      }
      if (muon->isAvailable<float>("EnergyLoss"))
        energyLoss = muon->auxdata<float>("EnergyLoss"); // In Run 2, use ParamEnergyLoss
      if (calE > 0 && energyLoss > 0)
        calERel = calE / energyLoss;
      */

      if(muon->clusterLink().isValid()) {
        const xAOD::CaloCluster* cluster = *(muon->clusterLink());
        float Energyloss = acc_energyloss(*muon);
        float calorimeterE = acc_calorimeterE(*cluster);
        //std::cout << calorimeterE << ", " << Energyloss <<std::endl;
        energyLoss = Energyloss;
        calE = calorimeterE;
        calERel = calorimeterE / Energyloss;
      }

      m_mu_calE.push_back(calE);
      m_mu_calERel.push_back(calERel);
      m_mu_dRlj.push_back(dRlj);
      m_mu_dRljAll.push_back(dRljAll);
      m_mu_EnergyLoss.push_back(energyLoss);
      m_mu_iffClass.push_back(iffClass);
      m_mu_hasTrackJet.push_back(hasTrackJet);
      m_mu_ptFrac.push_back(ptFrac);
      m_mu_ptRaw.push_back(ptRaw);
      m_mu_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500.push_back(ptvarcone30);
      m_mu_ptvarcone30Rel.push_back(ptvarcone30Rel);
      m_mu_topoetcone30.push_back(topoetcone30);
      m_mu_topoetcone30Rel.push_back(topoetcone30Rel);
      m_mu_topoetcone40.push_back(topoetcone40);
      m_mu_topoetcone40Rel.push_back(topoetcone40Rel);

      // find and store track muon information
      float trackMuDrTrackJet = -999.;
      float trackMuPtRelTrackJet = -999.;
      float trackMuPt = -999.;
      float trackMuEta = -999.;
      float trackMuPhi = -999.;
      float trackMuE = -999.;
      float trackMuD0 = -999.;
      float trackMuD0Sig = -999.;
      uint8_t trackMuNPixelHits = 0;
      uint8_t trackMuNPixelHoles = 0;
      uint8_t trackMuNPixelSharedHits = 0;
      uint8_t trackMuNSCTHits = 0;
      uint8_t trackMuNSCTHoles = 0;
      uint8_t trackMuNSCTSharedHits = 0;
      uint8_t trackMuNSiHits = 0; // nPixelHits + nSCTHits
      uint8_t trackMuNSiHoles = 0;
      float trackMuNSiSharedHits = 0; // nPixelSharedHits + nSCTSharedHits / 2
      float trackMuZ0 = -999.;
      float trackMuZ0Sig = -999.;
      float trackMuZ0Sin = -999.;

      auto trackMuon = findTrackMuon(muon);

      if (trackMuon && checkTrackMuon(trackMuon))
      {
        hasTrackMuon = 1;
        trackMuPt = trackMuon->pt();
        trackMuEta = trackMuon->eta();
        trackMuPhi = trackMuon->phi();
        trackMuE = trackMuon->e();
        trackMuD0 = trackMuon->d0();
        trackMuD0Sig = xAOD::TrackingHelpers::d0significance(trackMuon);
        if (trackMuon->isAvailable<uint8_t>("numberOfPixelHits"))
          trackMuNPixelHits = trackMuon->auxdata<uint8_t>("numberOfPixelHits");
        if (trackMuon->isAvailable<uint8_t>("numberOfPixelHoles"))
          trackMuNPixelHoles = trackMuon->auxdata<uint8_t>("numberOfPixelHoles");
        if (trackMuon->isAvailable<uint8_t>("numberOfPixelSharedHits"))
          trackMuNPixelSharedHits = trackMuon->auxdata<uint8_t>("numberOfPixelSharedHits");
        if (trackMuon->isAvailable<uint8_t>("numberOfSCTHits"))
          trackMuNSCTHits = trackMuon->auxdata<uint8_t>("numberOfSCTHits");
        if (trackMuon->isAvailable<uint8_t>("numberOfSCTHoles"))
          trackMuNSCTHits = trackMuon->auxdata<uint8_t>("numberOfSCTHoles");
        if (trackMuon->isAvailable<uint8_t>("numberOfSCTSharedHits"))
          trackMuNSCTSharedHits = trackMuon->auxdata<uint8_t>("numberOfSCTSharedHits");
        trackMuNSiHits = trackMuNPixelHits + trackMuNSCTHits;
        trackMuNSiHoles = trackMuNPixelHoles + trackMuNSCTHoles;
        trackMuNSiSharedHits = float(trackMuNPixelSharedHits) + float(trackMuNSCTSharedHits) / 2;
        trackMuZ0 = trackMuon->z0();
        trackMuZ0Sig = xAOD::TrackingHelpers::z0significance(trackMuon);
        if (primaryVertex)
        {
          const float deltaZ0 = trackMuon->z0() + trackMuon->vz() - primaryVertex->z();
          trackMuZ0Sin = deltaZ0 * TMath::Sin(trackMuon->theta());
        }
      }

      auto closestTrackJetTrackMuon = PLIV::findClosestJet(*trackMuon, event.m_trackJets, m_maxLepTrackJetDr);

      if (closestTrackJetTrackMuon)
      {
        trackMuonHasTrackJet = 1;
        trackMuDrTrackJet = trackMuon->p4().DeltaR(muon->p4());
        trackJetPt = closestTrackJetTrackMuon->pt();
        trackMuPtRelTrackJet = trackMuPt / trackJetPt;
        trackJetEta = closestTrackJetTrackMuon->eta();
        trackJetPhi = closestTrackJetTrackMuon->phi();
        trackJetE = closestTrackJetTrackMuon->e();
      }
      m_mu_hasTrackMuon.push_back(hasTrackMuon);
      m_trackMu_pt.push_back(trackMuPt);
      m_trackMu_eta.push_back(trackMuEta);
      m_trackMu_phi.push_back(trackMuPhi);
      m_trackMu_e.push_back(trackMuE);
      m_trackMu_d0.push_back(trackMuD0);
      m_trackMu_d0Sig.push_back(trackMuD0Sig);
      m_trackMu_dRTrackJet.push_back(trackMuDrTrackJet);
      m_trackMu_nPixelHits.push_back(trackMuNPixelHits);
      m_trackMu_nPixelHoles.push_back(trackMuNPixelHoles);
      m_trackMu_nPixelSharedHits.push_back(trackMuNPixelSharedHits);
      m_trackMu_nSCTHits.push_back(trackMuNSCTHits);
      m_trackMu_nSCTHoles.push_back(trackMuNSCTHoles);
      m_trackMu_nSCTSharedHits.push_back(trackMuNSCTSharedHits);
      m_trackMu_nSiHits.push_back(trackMuNSiHits);
      m_trackMu_nSiHoles.push_back(trackMuNSiHoles);
      m_trackMu_nSiSharedHits.push_back(trackMuNSiSharedHits);
      m_trackMu_ptRelTrackJet.push_back(trackMuPtRelTrackJet);
      m_trackMu_z0.push_back(trackMuZ0);
      m_trackMu_z0Sig.push_back(trackMuZ0Sig);
      m_trackMu_z0Sin.push_back(trackMuZ0Sin);
      m_trackMu_hasTrackJet.push_back(trackMuonHasTrackJet);
      m_trackMuTrackJet_pt.push_back(trackJetPt);
      m_trackMuTrackJet_eta.push_back(trackJetEta);
      m_trackMuTrackJet_phi.push_back(trackJetPhi);
      m_trackMuTrackJet_e.push_back(trackJetE);
    }
  }

  void DecorateMuons::SetBranches(std::shared_ptr<top::TreeManager> tree)
  {
    tree->makeOutputVariable(m_mu_calE, "mu_calE");
    tree->makeOutputVariable(m_mu_calERel, "mu_calERel");
    tree->makeOutputVariable(m_mu_dRlj, "mu_dRlj");
    tree->makeOutputVariable(m_mu_dRljAll, "mu_dRljAll");
    tree->makeOutputVariable(m_mu_EnergyLoss, "mu_energyLoss");
    tree->makeOutputVariable(m_mu_hasTrackJet, "mu_hasTrackJet");
    tree->makeOutputVariable(m_mu_hasTrackMuon, "mu_hasTrackMuon");
    tree->makeOutputVariable(m_mu_iffClass, "mu_iffClass");
    tree->makeOutputVariable(m_mu_ptFrac, "mu_ptFrac");
    tree->makeOutputVariable(m_mu_ptRaw, "mu_ptRaw");
    tree->makeOutputVariable(m_mu_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500, "mu_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500");
    tree->makeOutputVariable(m_mu_ptvarcone30Rel, "mu_ptvarcone30Rel");
    tree->makeOutputVariable(m_mu_topoetcone30, "mu_topoetcone30");
    tree->makeOutputVariable(m_mu_topoetcone30Rel, "mu_topoetcone30Rel");
    tree->makeOutputVariable(m_mu_topoetcone40, "mu_topoetcone40");
    tree->makeOutputVariable(m_mu_topoetcone40Rel, "mu_topoetcone40Rel");
    tree->makeOutputVariable(m_trackMu_pt, "trackMu_pt");
    tree->makeOutputVariable(m_trackMu_eta, "trackMu_eta");
    tree->makeOutputVariable(m_trackMu_phi, "trackMu_phi");
    tree->makeOutputVariable(m_trackMu_e, "trackMu_e");
    tree->makeOutputVariable(m_trackMu_hasTrackJet, "trackMu_hasTrackJet");
    tree->makeOutputVariable(m_trackMu_d0, "trackMu_d0");
    tree->makeOutputVariable(m_trackMu_d0Sig, "trackMu_d0Sig");
    tree->makeOutputVariable(m_trackMu_dRTrackJet, "trackMu_dRTrackJet");
    tree->makeOutputVariable(m_trackMu_nPixelHits, "trackMu_nPixelHits");
    tree->makeOutputVariable(m_trackMu_nPixelHoles, "trackMu_nPixelHoles");
    tree->makeOutputVariable(m_trackMu_nPixelSharedHits, "trackMu_nPixelSharedHits");
    tree->makeOutputVariable(m_trackMu_nSCTHits, "trackMu_nSCTHits");
    tree->makeOutputVariable(m_trackMu_nSCTHoles, "trackMu_nSCTHoles");
    tree->makeOutputVariable(m_trackMu_nSCTSharedHits, "trackMu_nSCTSharedHits");
    tree->makeOutputVariable(m_trackMu_nSiHits, "trackMu_nSiHits");
    tree->makeOutputVariable(m_trackMu_nSiHoles, "trackMu_nSiHoles");
    tree->makeOutputVariable(m_trackMu_ptRelTrackJet, "trackMu_ptRelTrackJet");
    tree->makeOutputVariable(m_trackMu_z0, "trackMu_z0");
    tree->makeOutputVariable(m_trackMu_z0Sig, "trackMu_z0Sig");
    tree->makeOutputVariable(m_trackMu_z0Sin, "trackMu_z0Sin");
    tree->makeOutputVariable(m_trackMuTrackJet_pt, "trackMu_trackJetPt");
    tree->makeOutputVariable(m_trackMuTrackJet_eta, "trackMu_trackJetEta");
    tree->makeOutputVariable(m_trackMuTrackJet_phi, "trackMu_trackJetPhi");
    tree->makeOutputVariable(m_trackMuTrackJet_e, "trackMu_trackJetE");
  }

  bool DecorateMuons::checkTrackMuon(const xAOD::TrackParticle *trackLep)
  {
    if (!trackLep)
      return false;

    uint8_t numberOfPixelHits = 0;
    uint8_t numberOfSCTHits = 0;
    uint8_t numberOfPixelHoles = 0;
    uint8_t numberOfSCTHoles = 0;
    uint8_t numberOfPixelSharedHits = 0;
    uint8_t numberOfSCTSharedHits = 0;

    if (!(trackLep->summaryValue(numberOfPixelHits, xAOD::numberOfPixelHits)))
      return false;
    if (!(trackLep->summaryValue(numberOfSCTHits, xAOD::numberOfSCTHits)))
      return false;
    if (!(trackLep->summaryValue(numberOfPixelHoles, xAOD::numberOfPixelHoles)))
      return false;
    if (!(trackLep->summaryValue(numberOfSCTHoles, xAOD::numberOfSCTHoles)))
      return false;
    if (!(trackLep->summaryValue(numberOfPixelSharedHits, xAOD::numberOfPixelSharedHits)))
      return false;
    if (!(trackLep->summaryValue(numberOfSCTSharedHits, xAOD::numberOfSCTSharedHits)))
      return false;
    
    return true;
  }

  const xAOD::TrackParticle* DecorateMuons::findTrackMuon(const xAOD::Muon* muon)
  {
    if (muon->muonType() != xAOD::Muon::Combined || !muon->inDetTrackParticleLink().isValid())
    {
      return nullptr;
    }
    const xAOD::TrackParticle *trackMuon = *(muon->inDetTrackParticleLink());
    return trackMuon;
  }
}
