#include "PLIVTupleMaker/DecorateLeptons.h"

#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h"
#include "TopEventSelectionTools/TreeManager.h"
#include "AsgTools/AnaToolHandle.h"

#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODEgamma/EgammaxAODHelpers.h"
#include "xAODCaloEvent/CaloClusterContainer.h"

#include "PLIVTupleMaker/PLIVUtils.h"

#include "TMath.h"

#include <vector>

namespace top{

    DecorateLeptons::DecorateLeptons(){
        m_maxLepTrackJetDr = 1.0;
        m_maxDeltaZ0Sin = 0.5;
        m_maxTopoEtCone30Rel = 0.3;

        m_total_leptons = -99;
        m_total_charge = -99;
        m_total_electrons = -99;
        m_total_muons = -99;

        m_electron_Pt_0 = -99;
        m_electron_Eta_0 = -99;
        m_electron_Phi_0 = -99;
        m_electron_E_0 = -99;
        m_electron_ID_0 = -99;
        m_electron_IFF_0 = -99;
        m_electron_isPrompt_0 = -99;

        m_electron_Pt_1 = -99;
        m_electron_Eta_1 = -99;
        m_electron_Phi_1 = -99;
        m_electron_E_1 = -99;
        m_electron_ID_1 = -99;
        m_electron_IFF_1 = -99;
        m_electron_isPrompt_1 = -99;

        m_electron_Pt_2 = -99;
        m_electron_Eta_2 = -99;
        m_electron_Phi_2 = -99;
        m_electron_E_2 = -99;
        m_electron_ID_2 = -99;
        m_electron_IFF_2 = -99;
        m_electron_isPrompt_2 = -99;

        m_electron_Pt_3 = -99;
        m_electron_Eta_3 = -99;
        m_electron_Phi_3 = -99;
        m_electron_E_3 = -99;
        m_electron_ID_3 = -99;
        m_electron_IFF_3 = -99;
        m_electron_isPrompt_3 = -99;

        m_muon_Pt_0 = -99;
        m_muon_Eta_0 = -99;
        m_muon_Phi_0 = -99;
        m_muon_E_0 = -99;
        m_muon_ID_0 = -99;
        m_muon_IFF_0 = -99;
        m_muon_isPrompt_0 = -99;
        m_muon_calE_0 = -99.;
        m_muon_calERel_0 = -99.;
        m_muon_dRlj_0 = -99.;
        m_muon_dRljAll_0 = -99.;
        m_muon_hasTrackMuon_0 = 0;
        m_muon_hasTrackJet_0 = 0;
        m_muon_energyLoss_0 = -99.;
        m_muon_ptFrac_0 = -99.;
        m_muon_ptRaw_0 = -99.;
        m_muon_ptvarcone30_0 = -99.;
        m_muon_ptvarcone30Rel_0 = -99.;
        m_muon_topoetcone30_0 = -99.;
        m_muon_topoetcone30Rel_0 = -99.;
        m_muon_topoetcone40_0 = -99.;
        m_muon_topoetcone40Rel_0 = -99.;
        // muon track
        m_muon_trackMuPt_0 = -999.;
        m_muon_trackMuEta_0 = -999.;
        m_muon_trackMuPhi_0 = -999.;
        m_muon_trackMuE_0 = -999.;
        m_muon_trackMuD0_0 = -999.;
        m_muon_trackMuD0Sig_0 = -999.;
        m_muon_trackMuNPixelHits_0 = 0;
        m_muon_trackMuNPixelHoles_0 = 0;
        m_muon_trackMuNPixelSharedHits_0 = 0;
        m_muon_trackMuNSCTHits_0 = 0;
        m_muon_trackMuNSCTHoles_0 = 0;
        m_muon_trackMuNSCTSharedHits_0 = 0;
        m_muon_trackMuNSiHits_0 = 0; // nPixelHits + nSCTHits
        m_muon_trackMuNSiHoles_0 = 0;
        m_muon_trackMuNSiSharedHits_0 = 0; // nPixelSharedHits + nSCTSharedHits / 2
        m_muon_trackMuZ0_0 = -999.;
        m_muon_trackMuZ0Sig_0 = -999.;
        m_muon_trackMuZ0Sin_0 = -999.;
        // trackjet matched to muon track
        m_muon_trackMuonHasTrackJet_0 = 0;
        m_muon_trackMuDrTrackJet_0 = -999.;
        m_muon_trackMuPtRelTrackJet_0 = -999.;
        m_muon_trackJetPt_0 = -99.;
        m_muon_trackJetEta_0 = -99.;
        m_muon_trackJetPhi_0 = -99.;
        m_muon_trackJetE_0 = -99.;

        m_muon_Pt_1 = -99;
        m_muon_Eta_1 = -99;
        m_muon_Phi_1 = -99;
        m_muon_E_1 = -99;
        m_muon_ID_1 = -99;
        m_muon_IFF_1 = -99;
        m_muon_isPrompt_1 = -99;
        m_muon_calE_1 = -99.;
        m_muon_calERel_1 = -99.;
        m_muon_dRlj_1 = -99.;
        m_muon_dRljAll_1 = -99.;
        m_muon_hasTrackMuon_1 = 0;
        m_muon_hasTrackJet_1 = 0;
        m_muon_energyLoss_1 = -99.;
        m_muon_ptFrac_1 = -99.;
        m_muon_ptRaw_1 = -99.;
        m_muon_ptvarcone30_1 = -99.;
        m_muon_ptvarcone30Rel_1 = -99.;
        m_muon_topoetcone30_1 = -99.;
        m_muon_topoetcone30Rel_1 = -99.;
        m_muon_topoetcone40_1 = -99.;
        m_muon_topoetcone40Rel_1 = -99.;
        m_muon_trackMuonHasTrackJet_1 = 0;
        m_muon_trackJetPt_1 = -99.;
        m_muon_trackJetEta_1 = -99.;
        m_muon_trackJetPhi_1 = -99.;
        m_muon_trackJetE_1 = -99.;

        m_muon_Pt_2 = -99;
        m_muon_Eta_2 = -99;
        m_muon_Phi_2 = -99;
        m_muon_E_2 = -99;
        m_muon_ID_2 = -99;
        m_muon_IFF_2 = -99;
        m_muon_isPrompt_2 = -99;
        m_muon_calE_2 = -99.;
        m_muon_calERel_2 = -99.;
        m_muon_dRlj_2 = -99.;
        m_muon_dRljAll_2 = -99.;
        m_muon_hasTrackMuon_2 = 0;
        m_muon_hasTrackJet_2 = 0;
        m_muon_energyLoss_2 = -99.;
        m_muon_ptFrac_2 = -99.;
        m_muon_ptRaw_2 = -99.;
        m_muon_ptvarcone30_2 = -99.;
        m_muon_ptvarcone30Rel_2 = -99.;
        m_muon_topoetcone30_2 = -99.;
        m_muon_topoetcone30Rel_2 = -99.;
        m_muon_topoetcone40_2 = -99.;
        m_muon_topoetcone40Rel_2 = -99.;
        m_muon_trackMuonHasTrackJet_2 = 0;
        m_muon_trackJetPt_2 = -99.;
        m_muon_trackJetEta_2 = -99.;
        m_muon_trackJetPhi_2 = -99.;
        m_muon_trackJetE_2 = -99.;

        m_muon_Pt_3 = -99;
        m_muon_Eta_3 = -99;
        m_muon_Phi_3 = -99;
        m_muon_E_3 = -99;
        m_muon_ID_3 = -99;
        m_muon_IFF_3 = -99;
        m_muon_isPrompt_3 = -99;  
        m_muon_calE_3 = -99.;
        m_muon_calERel_3 = -99.;
        m_muon_dRlj_3 = -99.;
        m_muon_dRljAll_3 = -99.;
        m_muon_hasTrackMuon_3 = 0;
        m_muon_hasTrackJet_3 = 0;
        m_muon_energyLoss_3 = -99.;
        m_muon_ptFrac_3 = -99.;
        m_muon_ptRaw_3 = -99.;
        m_muon_ptvarcone30_3 = -99.;
        m_muon_ptvarcone30Rel_3 = -99.;
        m_muon_topoetcone30_3 = -99.;
        m_muon_topoetcone30Rel_3 = -99.;
        m_muon_topoetcone40_3 = -99.;
        m_muon_topoetcone40Rel_3 = -99.;
        m_muon_trackMuonHasTrackJet_3 = 0;
        m_muon_trackJetPt_3 = -99.;
        m_muon_trackJetEta_3 = -99.;
        m_muon_trackJetPhi_3 = -99.;
        m_muon_trackJetE_3 = -99.;  
    }

    DecorateLeptons::~DecorateLeptons(){
    }

    void DecorateLeptons::ProcessEvent(const top::Event &event, ToolHandle<CP::IClassificationTool>& iffTool){
        // Initialize the entries
        m_total_leptons = -99;
        m_total_charge = -99;
        m_total_electrons = -99;
        m_total_muons = -99;

        m_electron_Pt_0 = -99;
        m_electron_Eta_0 = -99;
        m_electron_Phi_0 = -99;
        m_electron_E_0 = -99;
        m_electron_ID_0 = -99;
        m_electron_IFF_0 = -99;
        m_electron_isPrompt_0 = -99;

        m_electron_Pt_1 = -99;
        m_electron_Eta_1 = -99;
        m_electron_Phi_1 = -99;
        m_electron_E_1 = -99;
        m_electron_ID_1 = -99;
        m_electron_IFF_1 = -99;
        m_electron_isPrompt_1 = -99;

        m_electron_Pt_2 = -99;
        m_electron_Eta_2 = -99;
        m_electron_Phi_2 = -99;
        m_electron_E_2 = -99;
        m_electron_ID_2 = -99;
        m_electron_IFF_2 = -99;
        m_electron_isPrompt_2 = -99;

        m_electron_Pt_3 = -99;
        m_electron_Eta_3 = -99;
        m_electron_Phi_3 = -99;
        m_electron_E_3 = -99;
        m_electron_ID_3 = -99;
        m_electron_IFF_3 = -99;
        m_electron_isPrompt_3 = -99;

        m_muon_Pt_0 = -99;
        m_muon_Eta_0 = -99;
        m_muon_Phi_0 = -99;
        m_muon_E_0 = -99;
        m_muon_ID_0 = -99;
        m_muon_IFF_0 = -99;
        m_muon_isPrompt_0 = -99;
        m_muon_calE_0 = -99.;
        m_muon_calERel_0 = -99.;
        m_muon_dRlj_0 = -99.;
        m_muon_dRljAll_0 = -99.;
        m_muon_hasTrackMuon_0 = 0;
        m_muon_hasTrackJet_0 = 0;
        m_muon_energyLoss_0 = -99.;
        m_muon_ptFrac_0 = -99.;
        m_muon_ptRaw_0 = -99.;
        m_muon_ptvarcone30_0 = -99.;
        m_muon_ptvarcone30Rel_0 = -99.;
        m_muon_topoetcone30_0 = -99.;
        m_muon_topoetcone30Rel_0 = -99.;
        m_muon_topoetcone40_0 = -99.;
        m_muon_topoetcone40Rel_0 = -99.;

        m_muon_trackMuPt_0 = -999.;
        m_muon_trackMuEta_0 = -999.;
        m_muon_trackMuPhi_0 = -999.;
        m_muon_trackMuE_0 = -999.;
        m_muon_trackMuD0_0 = -999.;
        m_muon_trackMuD0Sig_0 = -999.;
        m_muon_trackMuNPixelHits_0 = 0;
        m_muon_trackMuNPixelHoles_0 = 0;
        m_muon_trackMuNPixelSharedHits_0 = 0;
        m_muon_trackMuNSCTHits_0 = 0;
        m_muon_trackMuNSCTHoles_0 = 0;
        m_muon_trackMuNSCTSharedHits_0 = 0;
        m_muon_trackMuNSiHits_0 = 0; // nPixelHits + nSCTHits
        m_muon_trackMuNSiHoles_0 = 0;
        m_muon_trackMuNSiSharedHits_0 = 0; // nPixelSharedHits + nSCTSharedHits / 2
        m_muon_trackMuZ0_0 = -999.;
        m_muon_trackMuZ0Sig_0 = -999.;
        m_muon_trackMuZ0Sin_0 = -999.;

        m_muon_trackMuonHasTrackJet_0 = 0;
        m_muon_trackMuDrTrackJet_0 = -999.;
        m_muon_trackMuPtRelTrackJet_0 = -999.;
        m_muon_trackJetPt_0 = -99.;
        m_muon_trackJetEta_0 = -99.;
        m_muon_trackJetPhi_0 = -99.;
        m_muon_trackJetE_0 = -99.;

        m_muon_Pt_1 = -99;
        m_muon_Eta_1 = -99;
        m_muon_Phi_1 = -99;
        m_muon_E_1 = -99;
        m_muon_ID_1 = -99;
        m_muon_IFF_1 = -99;
        m_muon_isPrompt_1 = -99;
        m_muon_calE_1 = -99.;
        m_muon_calERel_1 = -99.;
        m_muon_dRlj_1 = -99.;
        m_muon_dRljAll_1 = -99.;
        m_muon_hasTrackMuon_1 = 0;
        m_muon_hasTrackJet_1 = 0;
        m_muon_energyLoss_1 = -99.;
        m_muon_ptFrac_1 = -99.;
        m_muon_ptRaw_1 = -99.;
        m_muon_ptvarcone30_1 = -99.;
        m_muon_ptvarcone30Rel_1 = -99.;
        m_muon_topoetcone30_1 = -99.;
        m_muon_topoetcone30Rel_1 = -99.;
        m_muon_topoetcone40_1 = -99.;
        m_muon_topoetcone40Rel_1 = -99.;
        m_muon_trackMuonHasTrackJet_1 = 0;
        m_muon_trackJetPt_1 = -99.;
        m_muon_trackJetEta_1 = -99.;
        m_muon_trackJetPhi_1 = -99.;
        m_muon_trackJetE_1 = -99.;

        m_muon_Pt_2 = -99;
        m_muon_Eta_2 = -99;
        m_muon_Phi_2 = -99;
        m_muon_E_2 = -99;
        m_muon_ID_2 = -99;
        m_muon_IFF_2 = -99;
        m_muon_isPrompt_2 = -99;
        m_muon_calE_2 = -99.;
        m_muon_calERel_2 = -99.;
        m_muon_dRlj_2 = -99.;
        m_muon_dRljAll_2 = -99.;
        m_muon_hasTrackMuon_2 = 0;
        m_muon_hasTrackJet_2 = 0;
        m_muon_energyLoss_2 = -99.;
        m_muon_ptFrac_2 = -99.;
        m_muon_ptRaw_2 = -99.;
        m_muon_ptvarcone30_2 = -99.;
        m_muon_ptvarcone30Rel_2 = -99.;
        m_muon_topoetcone30_2 = -99.;
        m_muon_topoetcone30Rel_2 = -99.;
        m_muon_topoetcone40_2 = -99.;
        m_muon_topoetcone40Rel_2 = -99.;
        m_muon_trackMuonHasTrackJet_2 = 0;
        m_muon_trackJetPt_2 = -99.;
        m_muon_trackJetEta_2 = -99.;
        m_muon_trackJetPhi_2 = -99.;
        m_muon_trackJetE_2 = -99.;

        m_muon_Pt_3 = -99;
        m_muon_Eta_3 = -99;
        m_muon_Phi_3 = -99;
        m_muon_E_3 = -99;
        m_muon_ID_3 = -99;
        m_muon_IFF_3 = -99;
        m_muon_isPrompt_3 = -99;  
        m_muon_calE_3 = -99.;
        m_muon_calERel_3 = -99.;
        m_muon_dRlj_3 = -99.;
        m_muon_dRljAll_3 = -99.;
        m_muon_hasTrackMuon_3 = 0;
        m_muon_hasTrackJet_3 = 0;
        m_muon_energyLoss_3 = -99.;
        m_muon_ptFrac_3 = -99.;
        m_muon_ptRaw_3 = -99.;
        m_muon_ptvarcone30_3 = -99.;
        m_muon_ptvarcone30Rel_3 = -99.;
        m_muon_topoetcone30_3 = -99.;
        m_muon_topoetcone30Rel_3 = -99.;
        m_muon_topoetcone40_3 = -99.;
        m_muon_topoetcone40Rel_3 = -99.;
        m_muon_trackMuonHasTrackJet_3 = 0;
        m_muon_trackJetPt_3 = -99.;
        m_muon_trackJetEta_3 = -99.;
        m_muon_trackJetPhi_3 = -99.;
        m_muon_trackJetE_3 = -99.;  

        const xAOD::Vertex *primaryVertex = PLIV::findPrimaryVertex(event.m_primaryVertices);

        m_total_leptons = event.m_electrons.size() + event.m_muons.size();
        m_total_electrons = event.m_electrons.size();
        m_total_muons = event.m_muons.size();

        int total_charge = 0;
        for(auto electron : event.m_electrons){
            total_charge += electron->charge();
        }
        for(auto muon : event.m_muons){
            total_charge += muon->charge();
        }
        m_total_charge = total_charge;

        //Electron Properties

        if(event.m_electrons.size() >= 1){
            m_electron_Pt_0 = event.m_electrons.at(0)->pt();
            m_electron_Eta_0 = event.m_electrons.at(0)->eta();
            m_electron_Phi_0 = event.m_electrons.at(0)->phi();
            m_electron_E_0 = event.m_electrons.at(0)->e();
            if(event.m_electrons.at(0)->charge() == -1) m_electron_ID_0 = 11;
            else if(event.m_electrons.at(0)->charge() == +1) m_electron_ID_0 = -11;

            unsigned int electron_IFF_0 = -99;
            top::check(iffTool->classify(*event.m_electrons.at(0), electron_IFF_0), "Fail to get IFF class of this electron");
            m_electron_IFF_0 = (int)electron_IFF_0;
            if(m_electron_IFF_0 == 2) m_electron_isPrompt_0 = 1;
            else m_electron_isPrompt_0 = 0;
        }

        if(event.m_electrons.size() >= 2){
            m_electron_Pt_1 = event.m_electrons.at(1)->pt();
            m_electron_Eta_1 = event.m_electrons.at(1)->eta();
            m_electron_Phi_1 = event.m_electrons.at(1)->phi();
            m_electron_E_1 = event.m_electrons.at(1)->e();
            if(event.m_electrons.at(1)->charge() == -1) m_electron_ID_1 = 11;
            else if(event.m_electrons.at(1)->charge() == +1) m_electron_ID_1 = -11;

            unsigned int electron_IFF_1 = -99;
            top::check(iffTool->classify(*event.m_electrons.at(1), electron_IFF_1), "Fail to get IFF class of this electron");
            m_electron_IFF_1 = (int)electron_IFF_1;
            if(m_electron_IFF_1 == 2) m_electron_isPrompt_1 = 1;
            else m_electron_isPrompt_1 = 0;
        }

        if(event.m_electrons.size() >= 3){
            m_electron_Pt_2 = event.m_electrons.at(2)->pt();
            m_electron_Eta_2 = event.m_electrons.at(2)->eta();
            m_electron_Phi_2 = event.m_electrons.at(2)->phi();
            m_electron_E_2 = event.m_electrons.at(2)->e();
            if(event.m_electrons.at(2)->charge() == -1) m_electron_ID_2 = 11;
            else if(event.m_electrons.at(2)->charge() == +1) m_electron_ID_2 = -11;

            unsigned int electron_IFF_2 = -99;
            top::check(iffTool->classify(*event.m_electrons.at(2), electron_IFF_2), "Fail to get IFF class of this electron");
            m_electron_IFF_2 = (int)electron_IFF_2;
            if(m_electron_IFF_2 == 2) m_electron_isPrompt_2 = 1;
            else m_electron_isPrompt_2 = 0;
        }

        if(event.m_electrons.size() >= 4){
            m_electron_Pt_3 = event.m_electrons.at(3)->pt();
            m_electron_Eta_3 = event.m_electrons.at(3)->eta();
            m_electron_Phi_3 = event.m_electrons.at(3)->phi();
            m_electron_E_3 = event.m_electrons.at(3)->e();
            if(event.m_electrons.at(3)->charge() == -1) m_electron_ID_3 = 11;
            else if(event.m_electrons.at(3)->charge() == +1) m_electron_ID_3 = -11;

            unsigned int electron_IFF_3 = -99;
            top::check(iffTool->classify(*event.m_electrons.at(3), electron_IFF_3), "Fail to get IFF class of this electron");
            m_electron_IFF_3 = (int)electron_IFF_3;
            if(m_electron_IFF_3 == 2) m_electron_isPrompt_3 = 1;
            else m_electron_isPrompt_3 = 0;
        }

        //Muon Properties
        static SG::AuxElement::ConstAccessor<float> acc_calorimeterE("calE");
        static SG::AuxElement::ConstAccessor<float> acc_energyloss("EnergyLoss");

        if(event.m_muons.size() >= 1){
            m_muon_Pt_0 = event.m_muons.at(0)->pt();
            m_muon_Eta_0 = event.m_muons.at(0)->eta();
            m_muon_Phi_0 = event.m_muons.at(0)->phi();
            m_muon_E_0 = event.m_muons.at(0)->e();
            if(event.m_muons.at(0)->charge() == -1) m_muon_ID_0 = 13;
            else if(event.m_muons.at(0)->charge() == +1) m_muon_ID_0 = -13;

            unsigned int muon_IFF_0 = -99;
            top::check(iffTool->classify(*event.m_muons.at(0), muon_IFF_0), "Fail to get IFF class of this muon");
            m_muon_IFF_0 = (int)muon_IFF_0;
            if(m_muon_IFF_0 == 4) m_muon_isPrompt_0 = 1;
            else m_muon_isPrompt_0 = 0;

            m_muon_ptRaw_0 = event.m_muons.at(0)->primaryTrackParticle()->pt();
            if (event.m_muons.at(0)->isAvailable<float>("ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500")){
                m_muon_ptvarcone30_0 = event.m_muons.at(0)->auxdata<float>("ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500");
                //std::cout<<"m_muon_ptvarcone30_0 = "<<m_muon_ptvarcone30_0<<std::endl;
                m_muon_ptvarcone30Rel_0 = m_muon_ptvarcone30_0 / m_muon_ptRaw_0;
            }
            if (event.m_muons.at(0)->isolation(m_muon_topoetcone30_0, xAOD::Iso::topoetcone30))
            {
                m_muon_topoetcone30Rel_0 = m_muon_topoetcone30_0 / m_muon_ptRaw_0;
            }
            else
            {
            ANA_MSG_WARNING("Can not retrieve topoetcone30");
            }
            if (event.m_muons.at(0)->isolation(m_muon_topoetcone40_0, xAOD::Iso::topoetcone40))
            {
                m_muon_topoetcone40Rel_0 = m_muon_topoetcone40_0 / m_muon_ptRaw_0;
            }
            else
            {
                ANA_MSG_WARNING("Can not retrieve topoetcone40");
            }

            auto closestTrackJet = top::PLIV::findClosestJet(*event.m_muons.at(0), event.m_trackJets, m_maxLepTrackJetDr);
            if (closestTrackJet){
                m_muon_hasTrackJet_0 = 1;
                m_muon_ptFrac_0 = m_muon_ptRaw_0 / closestTrackJet->pt();
                m_muon_dRlj_0 = closestTrackJet->p4().DeltaR(event.m_muons.at(0)->p4());
            }
            auto closestTrackJetAll = top::PLIV::findClosestJet(*event.m_muons.at(0), event.m_trackJets, std::numeric_limits<float>::max());
            if (closestTrackJetAll){
                m_muon_dRljAll_0 = closestTrackJetAll->p4().DeltaR(event.m_muons.at(0)->p4());
            }

            if(event.m_muons.at(0)->clusterLink().isValid()) {
                const xAOD::CaloCluster* cluster = *(event.m_muons.at(0)->clusterLink());
                m_muon_energyLoss_0 = acc_energyloss(*event.m_muons.at(0));
                m_muon_calE_0 = acc_calorimeterE(*cluster);
                m_muon_calERel_0 = m_muon_calE_0 / m_muon_energyLoss_0;
            }

            // find and store track muon information
            auto trackMuon = findTrackMuon(event.m_muons.at(0));

            if (trackMuon && checkTrackMuon(trackMuon))
            {
                m_muon_hasTrackMuon_0 = 1;
                m_muon_trackMuPt_0 = trackMuon->pt();
                m_muon_trackMuEta_0 = trackMuon->eta();
                m_muon_trackMuPhi_0 = trackMuon->phi();
                m_muon_trackMuE_0 = trackMuon->e();
                m_muon_trackMuD0_0 = trackMuon->d0();
                m_muon_trackMuD0Sig_0 = xAOD::TrackingHelpers::d0significance(trackMuon);
                if (trackMuon->isAvailable<uint8_t>("numberOfPixelHits"))
                    m_muon_trackMuNPixelHits_0 = trackMuon->auxdata<uint8_t>("numberOfPixelHits");
                if (trackMuon->isAvailable<uint8_t>("numberOfPixelHoles"))
                    m_muon_trackMuNPixelHoles_0 = trackMuon->auxdata<uint8_t>("numberOfPixelHoles");
                if (trackMuon->isAvailable<uint8_t>("numberOfPixelSharedHits"))
                    m_muon_trackMuNPixelSharedHits_0 = trackMuon->auxdata<uint8_t>("numberOfPixelSharedHits");
                if (trackMuon->isAvailable<uint8_t>("numberOfSCTHits"))
                    m_muon_trackMuNSCTHits_0 = trackMuon->auxdata<uint8_t>("numberOfSCTHits");
                if (trackMuon->isAvailable<uint8_t>("numberOfSCTHoles"))
                    m_muon_trackMuNSCTHoles_0 = trackMuon->auxdata<uint8_t>("numberOfSCTHoles");
                if (trackMuon->isAvailable<uint8_t>("numberOfSCTSharedHits"))
                    m_muon_trackMuNSCTSharedHits_0 = trackMuon->auxdata<uint8_t>("numberOfSCTSharedHits");
                m_muon_trackMuNSiHits_0 = m_muon_trackMuNPixelHits_0 + m_muon_trackMuNSCTHits_0;
                m_muon_trackMuNSiHoles_0 = m_muon_trackMuNPixelHoles_0 + m_muon_trackMuNSCTHoles_0;
                m_muon_trackMuNSiSharedHits_0 = float(m_muon_trackMuNPixelSharedHits_0) + float(m_muon_trackMuNSCTSharedHits_0) / 2;
                m_muon_trackMuZ0_0 = trackMuon->z0();
                m_muon_trackMuZ0Sig_0 = xAOD::TrackingHelpers::z0significance(trackMuon);
                if (primaryVertex)
                {
                    const float deltaZ0 = trackMuon->z0() + trackMuon->vz() - primaryVertex->z();
                    m_muon_trackMuZ0Sin_0 = deltaZ0 * TMath::Sin(trackMuon->theta());
                }

                auto closestTrackJetTrackMuon = PLIV::findClosestJet(*trackMuon, event.m_trackJets, m_maxLepTrackJetDr);

                if (closestTrackJetTrackMuon)
                {
                    m_muon_trackMuonHasTrackJet_0 = 1;
                    m_muon_trackMuDrTrackJet_0 = trackMuon->p4().DeltaR(closestTrackJetTrackMuon->p4());
                    m_muon_trackJetPt_0 = closestTrackJetTrackMuon->pt();
                    m_muon_trackMuPtRelTrackJet_0 = m_muon_trackMuPt_0 / m_muon_trackJetPt_0;
                    m_muon_trackJetEta_0 = closestTrackJetTrackMuon->eta();
                    m_muon_trackJetPhi_0 = closestTrackJetTrackMuon->phi();
                    m_muon_trackJetE_0 = closestTrackJetTrackMuon->e();
                }
            }
        }

        if(event.m_muons.size() >= 2){
            m_muon_Pt_1 = event.m_muons.at(1)->pt();
            m_muon_Eta_1 = event.m_muons.at(1)->eta();
            m_muon_Phi_1 = event.m_muons.at(1)->phi();
            m_muon_E_1 = event.m_muons.at(1)->e();
            if(event.m_muons.at(1)->charge() == -1) m_muon_ID_1 = 13;
            else if(event.m_muons.at(1)->charge() == +1) m_muon_ID_1 = -13;

            unsigned int muon_IFF_1 = -99;
            top::check(iffTool->classify(*event.m_muons.at(1), muon_IFF_1), "Fail to get IFF class of this muon");
            m_muon_IFF_1 = (int)muon_IFF_1;
            if(m_muon_IFF_1 == 4) m_muon_isPrompt_1 = 1;
            else m_muon_isPrompt_1 = 0;

            m_muon_ptRaw_1 = event.m_muons.at(1)->primaryTrackParticle()->pt();
            if (event.m_muons.at(1)->isAvailable<float>("ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500")){
                m_muon_ptvarcone30_1 = event.m_muons.at(1)->auxdata<float>("ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500");
                //std::cout<<"m_muon_ptvarcone30_1 = "<<m_muon_ptvarcone30_1<<std::endl;
                m_muon_ptvarcone30Rel_1 = m_muon_ptvarcone30_1 / m_muon_ptRaw_1;
            }
            if (event.m_muons.at(1)->isolation(m_muon_topoetcone30_1, xAOD::Iso::topoetcone30))
            {
                m_muon_topoetcone30Rel_1 = m_muon_topoetcone30_1 / m_muon_ptRaw_1;
            }
            else
            {
            ANA_MSG_WARNING("Can not retrieve topoetcone30");
            }
            if (event.m_muons.at(1)->isolation(m_muon_topoetcone40_1, xAOD::Iso::topoetcone40))
            {
                m_muon_topoetcone40Rel_1 = m_muon_topoetcone40_1 / m_muon_ptRaw_1;
            }
            else
            {
                ANA_MSG_WARNING("Can not retrieve topoetcone40");
            }

            auto closestTrackJet = top::PLIV::findClosestJet(*event.m_muons.at(1), event.m_trackJets, m_maxLepTrackJetDr);
            if (closestTrackJet){
                m_muon_hasTrackJet_1 = 1;
                m_muon_ptFrac_1 = m_muon_ptRaw_1 / closestTrackJet->pt();
                m_muon_dRlj_1 = closestTrackJet->p4().DeltaR(event.m_muons.at(1)->p4());
            }
            auto closestTrackJetAll = top::PLIV::findClosestJet(*event.m_muons.at(1), event.m_trackJets, std::numeric_limits<float>::max());
            if (closestTrackJetAll){
                m_muon_dRljAll_1 = closestTrackJetAll->p4().DeltaR(event.m_muons.at(1)->p4());
            }

            if(event.m_muons.at(1)->clusterLink().isValid()) {
                const xAOD::CaloCluster* cluster = *(event.m_muons.at(1)->clusterLink());
                m_muon_energyLoss_1 = acc_energyloss(*event.m_muons.at(1));
                m_muon_calE_1 = acc_calorimeterE(*cluster);
                m_muon_calERel_1 = m_muon_calE_1 / m_muon_energyLoss_1;
            }

            // find and store track muon information
            auto trackMuon = findTrackMuon(event.m_muons.at(1));

            if (trackMuon && checkTrackMuon(trackMuon))
            {
                m_muon_hasTrackMuon_1 = 1;
                m_muon_trackMuPt_1 = trackMuon->pt();
                m_muon_trackMuEta_1 = trackMuon->eta();
                m_muon_trackMuPhi_1 = trackMuon->phi();
                m_muon_trackMuE_1 = trackMuon->e();
                m_muon_trackMuD0_1 = trackMuon->d0();
                m_muon_trackMuD0Sig_1 = xAOD::TrackingHelpers::d0significance(trackMuon);
                if (trackMuon->isAvailable<uint8_t>("numberOfPixelHits"))
                    m_muon_trackMuNPixelHits_1 = trackMuon->auxdata<uint8_t>("numberOfPixelHits");
                if (trackMuon->isAvailable<uint8_t>("numberOfPixelHoles"))
                    m_muon_trackMuNPixelHoles_1 = trackMuon->auxdata<uint8_t>("numberOfPixelHoles");
                if (trackMuon->isAvailable<uint8_t>("numberOfPixelSharedHits"))
                    m_muon_trackMuNPixelSharedHits_1 = trackMuon->auxdata<uint8_t>("numberOfPixelSharedHits");
                if (trackMuon->isAvailable<uint8_t>("numberOfSCTHits"))
                    m_muon_trackMuNSCTHits_1 = trackMuon->auxdata<uint8_t>("numberOfSCTHits");
                if (trackMuon->isAvailable<uint8_t>("numberOfSCTHoles"))
                    m_muon_trackMuNSCTHoles_1 = trackMuon->auxdata<uint8_t>("numberOfSCTHoles");
                if (trackMuon->isAvailable<uint8_t>("numberOfSCTSharedHits"))
                    m_muon_trackMuNSCTSharedHits_1 = trackMuon->auxdata<uint8_t>("numberOfSCTSharedHits");
                m_muon_trackMuNSiHits_1 = m_muon_trackMuNPixelHits_1 + m_muon_trackMuNSCTHits_1;
                m_muon_trackMuNSiHoles_1 = m_muon_trackMuNPixelHoles_1 + m_muon_trackMuNSCTHoles_1;
                m_muon_trackMuNSiSharedHits_1 = float(m_muon_trackMuNPixelSharedHits_1) + float(m_muon_trackMuNSCTSharedHits_1) / 2;
                m_muon_trackMuZ0_1 = trackMuon->z0();
                m_muon_trackMuZ0Sig_1 = xAOD::TrackingHelpers::z0significance(trackMuon);
                if (primaryVertex)
                {
                    const float deltaZ0 = trackMuon->z0() + trackMuon->vz() - primaryVertex->z();
                    m_muon_trackMuZ0Sin_1 = deltaZ0 * TMath::Sin(trackMuon->theta());
                }

                auto closestTrackJetTrackMuon = PLIV::findClosestJet(*trackMuon, event.m_trackJets, m_maxLepTrackJetDr);

                if (closestTrackJetTrackMuon)
                {
                    m_muon_trackMuonHasTrackJet_1 = 1;
                    m_muon_trackMuDrTrackJet_1 = trackMuon->p4().DeltaR(closestTrackJetTrackMuon->p4());
                    m_muon_trackJetPt_1 = closestTrackJetTrackMuon->pt();
                    m_muon_trackMuPtRelTrackJet_1 = m_muon_trackMuPt_1 / m_muon_trackJetPt_1;
                    m_muon_trackJetEta_1 = closestTrackJetTrackMuon->eta();
                    m_muon_trackJetPhi_1 = closestTrackJetTrackMuon->phi();
                    m_muon_trackJetE_1 = closestTrackJetTrackMuon->e();
                }
            }
        }

        if(event.m_muons.size() >= 3){
            m_muon_Pt_2 = event.m_muons.at(2)->pt();
            m_muon_Eta_2 = event.m_muons.at(2)->eta();
            m_muon_Phi_2 = event.m_muons.at(2)->phi();
            m_muon_E_2 = event.m_muons.at(2)->e();
            if(event.m_muons.at(2)->charge() == -1) m_muon_ID_2 = 13;
            else if(event.m_muons.at(2)->charge() == +1) m_muon_ID_2 = -13;

            unsigned int muon_IFF_2 = -99;
            top::check(iffTool->classify(*event.m_muons.at(2), muon_IFF_2), "Fail to get IFF class of this muon");
            m_muon_IFF_2 = (int)muon_IFF_2;
            if(m_muon_IFF_2 == 4) m_muon_isPrompt_2 = 1;
            else m_muon_isPrompt_2 = 0;

            m_muon_ptRaw_2 = event.m_muons.at(2)->primaryTrackParticle()->pt();
            if (event.m_muons.at(2)->isAvailable<float>("ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500")){
                m_muon_ptvarcone30_2 = event.m_muons.at(2)->auxdata<float>("ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500");
                //std::cout<<"m_muon_ptvarcone30_2 = "<<m_muon_ptvarcone30_2<<std::endl;
                m_muon_ptvarcone30Rel_2 = m_muon_ptvarcone30_2 / m_muon_ptRaw_2;
            }
            if (event.m_muons.at(2)->isolation(m_muon_topoetcone30_2, xAOD::Iso::topoetcone30))
            {
                m_muon_topoetcone30Rel_2 = m_muon_topoetcone30_2 / m_muon_ptRaw_2;
            }
            else
            {
            ANA_MSG_WARNING("Can not retrieve topoetcone30");
            }
            if (event.m_muons.at(2)->isolation(m_muon_topoetcone40_2, xAOD::Iso::topoetcone40))
            {
                m_muon_topoetcone40Rel_2 = m_muon_topoetcone40_2 / m_muon_ptRaw_2;
            }
            else
            {
                ANA_MSG_WARNING("Can not retrieve topoetcone40");
            }

            auto closestTrackJet = top::PLIV::findClosestJet(*event.m_muons.at(2), event.m_trackJets, m_maxLepTrackJetDr);
            if (closestTrackJet){
                m_muon_hasTrackJet_2 = 1;
                m_muon_ptFrac_2 = m_muon_ptRaw_2 / closestTrackJet->pt();
                m_muon_dRlj_2 = closestTrackJet->p4().DeltaR(event.m_muons.at(2)->p4());
            }
            auto closestTrackJetAll = top::PLIV::findClosestJet(*event.m_muons.at(2), event.m_trackJets, std::numeric_limits<float>::max());
            if (closestTrackJetAll){
                m_muon_dRljAll_2 = closestTrackJetAll->p4().DeltaR(event.m_muons.at(2)->p4());
            }

            if(event.m_muons.at(2)->clusterLink().isValid()) {
                const xAOD::CaloCluster* cluster = *(event.m_muons.at(2)->clusterLink());
                m_muon_energyLoss_2 = acc_energyloss(*event.m_muons.at(2));
                m_muon_calE_2 = acc_calorimeterE(*cluster);
                m_muon_calERel_2 = m_muon_calE_2 / m_muon_energyLoss_2;
            }

            // find and store track muon information
            auto trackMuon = findTrackMuon(event.m_muons.at(2));

            if (trackMuon && checkTrackMuon(trackMuon))
            {
                m_muon_hasTrackMuon_2 = 1;
                m_muon_trackMuPt_2 = trackMuon->pt();
                m_muon_trackMuEta_2 = trackMuon->eta();
                m_muon_trackMuPhi_2 = trackMuon->phi();
                m_muon_trackMuE_2 = trackMuon->e();
                m_muon_trackMuD0_2 = trackMuon->d0();
                m_muon_trackMuD0Sig_2 = xAOD::TrackingHelpers::d0significance(trackMuon);
                if (trackMuon->isAvailable<uint8_t>("numberOfPixelHits"))
                    m_muon_trackMuNPixelHits_2 = trackMuon->auxdata<uint8_t>("numberOfPixelHits");
                if (trackMuon->isAvailable<uint8_t>("numberOfPixelHoles"))
                    m_muon_trackMuNPixelHoles_2 = trackMuon->auxdata<uint8_t>("numberOfPixelHoles");
                if (trackMuon->isAvailable<uint8_t>("numberOfPixelSharedHits"))
                    m_muon_trackMuNPixelSharedHits_2 = trackMuon->auxdata<uint8_t>("numberOfPixelSharedHits");
                if (trackMuon->isAvailable<uint8_t>("numberOfSCTHits"))
                    m_muon_trackMuNSCTHits_2 = trackMuon->auxdata<uint8_t>("numberOfSCTHits");
                if (trackMuon->isAvailable<uint8_t>("numberOfSCTHoles"))
                    m_muon_trackMuNSCTHoles_2 = trackMuon->auxdata<uint8_t>("numberOfSCTHoles");
                if (trackMuon->isAvailable<uint8_t>("numberOfSCTSharedHits"))
                    m_muon_trackMuNSCTSharedHits_2 = trackMuon->auxdata<uint8_t>("numberOfSCTSharedHits");
                m_muon_trackMuNSiHits_2 = m_muon_trackMuNPixelHits_2 + m_muon_trackMuNSCTHits_2;
                m_muon_trackMuNSiHoles_2 = m_muon_trackMuNPixelHoles_2 + m_muon_trackMuNSCTHoles_2;
                m_muon_trackMuNSiSharedHits_2 = float(m_muon_trackMuNPixelSharedHits_2) + float(m_muon_trackMuNSCTSharedHits_2) / 2;
                m_muon_trackMuZ0_2 = trackMuon->z0();
                m_muon_trackMuZ0Sig_2 = xAOD::TrackingHelpers::z0significance(trackMuon);
                if (primaryVertex)
                {
                    const float deltaZ0 = trackMuon->z0() + trackMuon->vz() - primaryVertex->z();
                    m_muon_trackMuZ0Sin_2 = deltaZ0 * TMath::Sin(trackMuon->theta());
                }

                auto closestTrackJetTrackMuon = PLIV::findClosestJet(*trackMuon, event.m_trackJets, m_maxLepTrackJetDr);

                if (closestTrackJetTrackMuon)
                {
                    m_muon_trackMuonHasTrackJet_2 = 1;
                    m_muon_trackMuDrTrackJet_2 = trackMuon->p4().DeltaR(closestTrackJetTrackMuon->p4());
                    m_muon_trackJetPt_2 = closestTrackJetTrackMuon->pt();
                    m_muon_trackMuPtRelTrackJet_2 = m_muon_trackMuPt_2 / m_muon_trackJetPt_2;
                    m_muon_trackJetEta_2 = closestTrackJetTrackMuon->eta();
                    m_muon_trackJetPhi_2 = closestTrackJetTrackMuon->phi();
                    m_muon_trackJetE_2 = closestTrackJetTrackMuon->e();
                }
            }
        }

        if(event.m_muons.size() >= 4){
            m_muon_Pt_3 = event.m_muons.at(3)->pt();
            m_muon_Eta_3 = event.m_muons.at(3)->eta();
            m_muon_Phi_3 = event.m_muons.at(3)->phi();
            m_muon_E_3 = event.m_muons.at(3)->e();
            if(event.m_muons.at(3)->charge() == -1) m_muon_ID_3 = 13;
            else if(event.m_muons.at(3)->charge() == +1) m_muon_ID_3 = -13;

            unsigned int muon_IFF_3 = -99;
            top::check(iffTool->classify(*event.m_muons.at(3), muon_IFF_3), "Fail to get IFF class of this muon");
            m_muon_IFF_3 = (int)muon_IFF_3;
            if(m_muon_IFF_3 == 4) m_muon_isPrompt_3 = 1;
            else m_muon_isPrompt_3 = 0;

            m_muon_ptRaw_3 = event.m_muons.at(3)->primaryTrackParticle()->pt();
            if (event.m_muons.at(3)->isAvailable<float>("ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500")){
                m_muon_ptvarcone30_3 = event.m_muons.at(3)->auxdata<float>("ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500");
                //std::cout<<"m_muon_ptvarcone30_3 = "<<m_muon_ptvarcone30_3<<std::endl;
                m_muon_ptvarcone30Rel_3 = m_muon_ptvarcone30_3 / m_muon_ptRaw_3;
            }
            if (event.m_muons.at(3)->isolation(m_muon_topoetcone30_3, xAOD::Iso::topoetcone30))
            {
                m_muon_topoetcone30Rel_3 = m_muon_topoetcone30_3 / m_muon_ptRaw_3;
            }
            else
            {
            ANA_MSG_WARNING("Can not retrieve topoetcone30");
            }
            if (event.m_muons.at(3)->isolation(m_muon_topoetcone40_3, xAOD::Iso::topoetcone40))
            {
                m_muon_topoetcone40Rel_3 = m_muon_topoetcone40_3 / m_muon_ptRaw_3;
            }
            else
            {
                ANA_MSG_WARNING("Can not retrieve topoetcone40");
            }

            auto closestTrackJet = top::PLIV::findClosestJet(*event.m_muons.at(3), event.m_trackJets, m_maxLepTrackJetDr);
            if (closestTrackJet){
                m_muon_hasTrackJet_3 = 1;
                m_muon_ptFrac_3 = m_muon_ptRaw_3 / closestTrackJet->pt();
                m_muon_dRlj_3 = closestTrackJet->p4().DeltaR(event.m_muons.at(3)->p4());
            }
            auto closestTrackJetAll = top::PLIV::findClosestJet(*event.m_muons.at(3), event.m_trackJets, std::numeric_limits<float>::max());
            if (closestTrackJetAll){
                m_muon_dRljAll_3 = closestTrackJetAll->p4().DeltaR(event.m_muons.at(3)->p4());
            }

            if(event.m_muons.at(3)->clusterLink().isValid()) {
                const xAOD::CaloCluster* cluster = *(event.m_muons.at(3)->clusterLink());
                m_muon_energyLoss_3 = acc_energyloss(*event.m_muons.at(3));
                m_muon_calE_3 = acc_calorimeterE(*cluster);
                m_muon_calERel_3 = m_muon_calE_3 / m_muon_energyLoss_3;
            }

            // find and store track muon information
            auto trackMuon = findTrackMuon(event.m_muons.at(3));

            if (trackMuon && checkTrackMuon(trackMuon))
            {
                m_muon_hasTrackMuon_3 = 1;
                m_muon_trackMuPt_3 = trackMuon->pt();
                m_muon_trackMuEta_3 = trackMuon->eta();
                m_muon_trackMuPhi_3 = trackMuon->phi();
                m_muon_trackMuE_3 = trackMuon->e();
                m_muon_trackMuD0_3 = trackMuon->d0();
                m_muon_trackMuD0Sig_3 = xAOD::TrackingHelpers::d0significance(trackMuon);
                if (trackMuon->isAvailable<uint8_t>("numberOfPixelHits"))
                    m_muon_trackMuNPixelHits_3 = trackMuon->auxdata<uint8_t>("numberOfPixelHits");
                if (trackMuon->isAvailable<uint8_t>("numberOfPixelHoles"))
                    m_muon_trackMuNPixelHoles_3 = trackMuon->auxdata<uint8_t>("numberOfPixelHoles");
                if (trackMuon->isAvailable<uint8_t>("numberOfPixelSharedHits"))
                    m_muon_trackMuNPixelSharedHits_3 = trackMuon->auxdata<uint8_t>("numberOfPixelSharedHits");
                if (trackMuon->isAvailable<uint8_t>("numberOfSCTHits"))
                    m_muon_trackMuNSCTHits_3 = trackMuon->auxdata<uint8_t>("numberOfSCTHits");
                if (trackMuon->isAvailable<uint8_t>("numberOfSCTHoles"))
                    m_muon_trackMuNSCTHoles_3 = trackMuon->auxdata<uint8_t>("numberOfSCTHoles");
                if (trackMuon->isAvailable<uint8_t>("numberOfSCTSharedHits"))
                    m_muon_trackMuNSCTSharedHits_3 = trackMuon->auxdata<uint8_t>("numberOfSCTSharedHits");
                m_muon_trackMuNSiHits_3 = m_muon_trackMuNPixelHits_3 + m_muon_trackMuNSCTHits_3;
                m_muon_trackMuNSiHoles_3 = m_muon_trackMuNPixelHoles_3 + m_muon_trackMuNSCTHoles_3;
                m_muon_trackMuNSiSharedHits_3 = float(m_muon_trackMuNPixelSharedHits_3) + float(m_muon_trackMuNSCTSharedHits_3) / 2;
                m_muon_trackMuZ0_3 = trackMuon->z0();
                m_muon_trackMuZ0Sig_3 = xAOD::TrackingHelpers::z0significance(trackMuon);
                if (primaryVertex)
                {
                    const float deltaZ0 = trackMuon->z0() + trackMuon->vz() - primaryVertex->z();
                    m_muon_trackMuZ0Sin_3 = deltaZ0 * TMath::Sin(trackMuon->theta());
                }

                auto closestTrackJetTrackMuon = PLIV::findClosestJet(*trackMuon, event.m_trackJets, m_maxLepTrackJetDr);

                if (closestTrackJetTrackMuon)
                {
                    m_muon_trackMuonHasTrackJet_3 = 1;
                    m_muon_trackMuDrTrackJet_3 = trackMuon->p4().DeltaR(closestTrackJetTrackMuon->p4());
                    m_muon_trackJetPt_3 = closestTrackJetTrackMuon->pt();
                    m_muon_trackMuPtRelTrackJet_3 = m_muon_trackMuPt_3 / m_muon_trackJetPt_3;
                    m_muon_trackJetEta_3 = closestTrackJetTrackMuon->eta();
                    m_muon_trackJetPhi_3 = closestTrackJetTrackMuon->phi();
                    m_muon_trackJetE_3 = closestTrackJetTrackMuon->e();
                }
            }
        }
    }

    void DecorateLeptons::SetBranches(std::shared_ptr<top::TreeManager> tree){
        tree->makeOutputVariable(m_total_leptons, "total_leptons");
        tree->makeOutputVariable(m_total_charge, "total_charge");
        tree->makeOutputVariable(m_total_muons, "total_muons");
        tree->makeOutputVariable(m_total_electrons, "total_electrons");

        tree->makeOutputVariable(m_electron_Pt_0, "electron_Pt_0");
        tree->makeOutputVariable(m_electron_Eta_0, "electron_Eta_0");
        tree->makeOutputVariable(m_electron_Phi_0, "electron_Phi_0");
        tree->makeOutputVariable(m_electron_E_0, "electron_E_0");
        tree->makeOutputVariable(m_electron_ID_0, "electron_ID_0");
        tree->makeOutputVariable(m_electron_IFF_0, "electron_IFF_0");
        tree->makeOutputVariable(m_electron_isPrompt_0, "electron_isPrompt_0");

        tree->makeOutputVariable(m_electron_Pt_1, "electron_Pt_1");
        tree->makeOutputVariable(m_electron_Eta_1, "electron_Eta_1");
        tree->makeOutputVariable(m_electron_Phi_1, "electron_Phi_1");
        tree->makeOutputVariable(m_electron_E_1, "electron_E_1");
        tree->makeOutputVariable(m_electron_ID_1, "electron_ID_1");
        tree->makeOutputVariable(m_electron_IFF_1, "electron_IFF_1");
        tree->makeOutputVariable(m_electron_isPrompt_1, "electron_isPrompt_1");

        tree->makeOutputVariable(m_electron_Pt_2, "electron_Pt_2");
        tree->makeOutputVariable(m_electron_Eta_2, "electron_Eta_2");
        tree->makeOutputVariable(m_electron_Phi_2, "electron_Phi_2");
        tree->makeOutputVariable(m_electron_E_2, "electron_E_2");
        tree->makeOutputVariable(m_electron_ID_2, "electron_ID_2");
        tree->makeOutputVariable(m_electron_IFF_2, "electron_IFF_2");
        tree->makeOutputVariable(m_electron_isPrompt_2, "electron_isPrompt_2");

        tree->makeOutputVariable(m_electron_Pt_3, "electron_Pt_3");
        tree->makeOutputVariable(m_electron_Eta_3, "electron_Eta_3");
        tree->makeOutputVariable(m_electron_Phi_3, "electron_Phi_3");
        tree->makeOutputVariable(m_electron_E_3, "electron_E_3");
        tree->makeOutputVariable(m_electron_ID_3, "electron_ID_3");
        tree->makeOutputVariable(m_electron_IFF_3, "electron_IFF_3");
        tree->makeOutputVariable(m_electron_isPrompt_3, "electron_isPrompt_3");

        tree->makeOutputVariable(m_muon_Pt_0, "muon_Pt_0");
        tree->makeOutputVariable(m_muon_Eta_0, "muon_Eta_0");
        tree->makeOutputVariable(m_muon_Phi_0, "muon_Phi_0");
        tree->makeOutputVariable(m_muon_E_0, "muon_E_0");
        tree->makeOutputVariable(m_muon_ID_0, "muon_ID_0");
        tree->makeOutputVariable(m_muon_IFF_0, "muon_IFF_0");
        tree->makeOutputVariable(m_muon_isPrompt_0, "muon_isPrompt_0");
        tree->makeOutputVariable(m_muon_calE_0, "muon_calE_0");
        tree->makeOutputVariable(m_muon_calERel_0, "muon_calERel_0");
        tree->makeOutputVariable(m_muon_dRlj_0, "muon_dRlj_0");
        tree->makeOutputVariable(m_muon_dRljAll_0, "muon_dRljAll_0");
        tree->makeOutputVariable(m_muon_hasTrackMuon_0, "muon_hasTrackMuon_0");
        tree->makeOutputVariable(m_muon_hasTrackJet_0, "muon_hasTrackJet_0");
        tree->makeOutputVariable(m_muon_energyLoss_0, "muon_energyLoss_0");
        tree->makeOutputVariable(m_muon_ptFrac_0, "muon_ptFrac_0");
        tree->makeOutputVariable(m_muon_ptRaw_0, "muon_ptRaw_0");
        tree->makeOutputVariable(m_muon_ptvarcone30_0, "muon_ptvarcone30_0");
        tree->makeOutputVariable(m_muon_ptvarcone30Rel_0, "muon_ptvarcone30Rel_0");
        tree->makeOutputVariable(m_muon_topoetcone30_0, "muon_topoetcone30_0");
        tree->makeOutputVariable(m_muon_topoetcone30Rel_0, "muon_topoetcone30Rel_0");
        tree->makeOutputVariable(m_muon_topoetcone40_0, "muon_topoetcone40_0");
        tree->makeOutputVariable(m_muon_topoetcone40Rel_0, "muon_topoetcone40Rel_0");
        // muon track
        tree->makeOutputVariable(m_muon_trackMuPt_0, "muon_trackMuPt_0");
        tree->makeOutputVariable(m_muon_trackMuEta_0, "muon_trackMuEta_0");
        tree->makeOutputVariable(m_muon_trackMuPhi_0, "muon_trackMuPhi_0");
        tree->makeOutputVariable(m_muon_trackMuE_0, "muon_trackMuE_0");
        tree->makeOutputVariable(m_muon_trackMuD0_0, "muon_trackMuD0_0");
        tree->makeOutputVariable(m_muon_trackMuD0Sig_0, "muon_trackMuD0Sig_0");
        tree->makeOutputVariable(m_muon_trackMuNPixelHits_0, "muon_trackMuNPixelHits_0");
        tree->makeOutputVariable(m_muon_trackMuNPixelHoles_0, "muon_trackMuNPixelHoles_0");
        tree->makeOutputVariable(m_muon_trackMuNPixelSharedHits_0, "muon_trackMuNPixelSharedHits_0");
        tree->makeOutputVariable(m_muon_trackMuNSCTHits_0, "muon_trackMuNSCTHits_0");
        tree->makeOutputVariable(m_muon_trackMuNSCTHoles_0, "muon_trackMuNSCTHoles_0");
        tree->makeOutputVariable(m_muon_trackMuNSCTSharedHits_0, "muon_trackMuNSCTSharedHits_0");
        tree->makeOutputVariable(m_muon_trackMuNSiHits_0, "muon_trackMuNSiHits_0");
        tree->makeOutputVariable(m_muon_trackMuNSiHoles_0, "muon_trackMuNSiHoles_0");
        tree->makeOutputVariable(m_muon_trackMuNSiSharedHits_0, "muon_trackMuNSiSharedHits_0");
        tree->makeOutputVariable(m_muon_trackMuZ0_0, "muon_trackMuZ0_0");
        tree->makeOutputVariable(m_muon_trackMuZ0Sig_0, "muon_trackMuZ0Sig_0");
        tree->makeOutputVariable(m_muon_trackMuZ0Sin_0, "muon_trackMuZ0Sin_0");
        // trackjet matched to muon track
        tree->makeOutputVariable(m_muon_trackMuonHasTrackJet_0, "muon_trackMuonHasTrackJet_0");
        tree->makeOutputVariable(m_muon_trackMuDrTrackJet_0, "muon_trackMuDrTrackJet_0");
        tree->makeOutputVariable(m_muon_trackMuPtRelTrackJet_0, "muon_trackMuPtRelTrackJet_0");
        tree->makeOutputVariable(m_muon_trackJetPt_0, "muon_trackJetPt_0");
        tree->makeOutputVariable(m_muon_trackJetEta_0, "muon_trackJetEta_0");
        tree->makeOutputVariable(m_muon_trackJetPhi_0, "muon_trackJetPhi_0");
        tree->makeOutputVariable(m_muon_trackJetE_0, "muon_trackJetE_0");

        tree->makeOutputVariable(m_muon_Pt_1, "muon_Pt_1");
        tree->makeOutputVariable(m_muon_Eta_1, "muon_Eta_1");
        tree->makeOutputVariable(m_muon_Phi_1, "muon_Phi_1");
        tree->makeOutputVariable(m_muon_E_1, "muon_E_1");
        tree->makeOutputVariable(m_muon_ID_1, "muon_ID_1");
        tree->makeOutputVariable(m_muon_IFF_1, "muon_IFF_1");
        tree->makeOutputVariable(m_muon_isPrompt_1, "muon_isPrompt_1");
        tree->makeOutputVariable(m_muon_calE_1, "muon_calE_1");
        tree->makeOutputVariable(m_muon_calERel_1, "muon_calERel_1");
        tree->makeOutputVariable(m_muon_dRlj_1, "muon_dRlj_1");
        tree->makeOutputVariable(m_muon_dRljAll_1, "muon_dRljAll_1");
        tree->makeOutputVariable(m_muon_hasTrackMuon_1, "muon_hasTrackMuon_1");
        tree->makeOutputVariable(m_muon_hasTrackJet_1, "muon_hasTrackJet_1");
        tree->makeOutputVariable(m_muon_energyLoss_1, "muon_energyLoss_1");
        tree->makeOutputVariable(m_muon_ptFrac_1, "muon_ptFrac_1");
        tree->makeOutputVariable(m_muon_ptRaw_1, "muon_ptRaw_1");
        tree->makeOutputVariable(m_muon_ptvarcone30_1, "muon_ptvarcone30_1");
        tree->makeOutputVariable(m_muon_ptvarcone30Rel_1, "muon_ptvarcone30Rel_1");
        tree->makeOutputVariable(m_muon_topoetcone30_1, "muon_topoetcone30_1");
        tree->makeOutputVariable(m_muon_topoetcone30Rel_1, "muon_topoetcone30Rel_1");
        tree->makeOutputVariable(m_muon_topoetcone40_1, "muon_topoetcone40_1");
        tree->makeOutputVariable(m_muon_topoetcone40Rel_1, "muon_topoetcone40Rel_1");
        // muon track
        tree->makeOutputVariable(m_muon_trackMuPt_1, "muon_trackMuPt_1");
        tree->makeOutputVariable(m_muon_trackMuEta_1, "muon_trackMuEta_1");
        tree->makeOutputVariable(m_muon_trackMuPhi_1, "muon_trackMuPhi_1");
        tree->makeOutputVariable(m_muon_trackMuE_1, "muon_trackMuE_1");
        tree->makeOutputVariable(m_muon_trackMuD0_1, "muon_trackMuD0_1");
        tree->makeOutputVariable(m_muon_trackMuD0Sig_1, "muon_trackMuD0Sig_1");
        tree->makeOutputVariable(m_muon_trackMuNPixelHits_1, "muon_trackMuNPixelHits_1");
        tree->makeOutputVariable(m_muon_trackMuNPixelHoles_1, "muon_trackMuNPixelHoles_1");
        tree->makeOutputVariable(m_muon_trackMuNPixelSharedHits_1, "muon_trackMuNPixelSharedHits_1");
        tree->makeOutputVariable(m_muon_trackMuNSCTHits_1, "muon_trackMuNSCTHits_1");
        tree->makeOutputVariable(m_muon_trackMuNSCTHoles_1, "muon_trackMuNSCTHoles_1");
        tree->makeOutputVariable(m_muon_trackMuNSCTSharedHits_1, "muon_trackMuNSCTSharedHits_1");
        tree->makeOutputVariable(m_muon_trackMuNSiHits_1, "muon_trackMuNSiHits_1");
        tree->makeOutputVariable(m_muon_trackMuNSiHoles_1, "muon_trackMuNSiHoles_1");
        tree->makeOutputVariable(m_muon_trackMuNSiSharedHits_1, "muon_trackMuNSiSharedHits_1");
        tree->makeOutputVariable(m_muon_trackMuZ0_1, "muon_trackMuZ0_1");
        tree->makeOutputVariable(m_muon_trackMuZ0Sig_1, "muon_trackMuZ0Sig_1");
        tree->makeOutputVariable(m_muon_trackMuZ0Sin_1, "muon_trackMuZ0Sin_1");
        // trackjet matched to muon track
        tree->makeOutputVariable(m_muon_trackMuonHasTrackJet_1, "muon_trackMuonHasTrackJet_1");
        tree->makeOutputVariable(m_muon_trackMuDrTrackJet_1, "muon_trackMuDrTrackJet_1");
        tree->makeOutputVariable(m_muon_trackMuPtRelTrackJet_1, "muon_trackMuPtRelTrackJet_1");
        tree->makeOutputVariable(m_muon_trackJetPt_1, "muon_trackJetPt_1");
        tree->makeOutputVariable(m_muon_trackJetEta_1, "muon_trackJetEta_1");
        tree->makeOutputVariable(m_muon_trackJetPhi_1, "muon_trackJetPhi_1");
        tree->makeOutputVariable(m_muon_trackJetE_1, "muon_trackJetE_1");

        tree->makeOutputVariable(m_muon_Pt_2, "muon_Pt_2");
        tree->makeOutputVariable(m_muon_Eta_2, "muon_Eta_2");
        tree->makeOutputVariable(m_muon_Phi_2, "muon_Phi_2");
        tree->makeOutputVariable(m_muon_E_2, "muon_E_2");
        tree->makeOutputVariable(m_muon_ID_2, "muon_ID_2");
        tree->makeOutputVariable(m_muon_IFF_2, "muon_IFF_2");
        tree->makeOutputVariable(m_muon_isPrompt_2, "muon_isPrompt_2");
        tree->makeOutputVariable(m_muon_calE_2, "muon_calE_2");
        tree->makeOutputVariable(m_muon_calERel_2, "muon_calERel_2");
        tree->makeOutputVariable(m_muon_dRlj_2, "muon_dRlj_2");
        tree->makeOutputVariable(m_muon_dRljAll_2, "muon_dRljAll_2");
        tree->makeOutputVariable(m_muon_hasTrackMuon_2, "muon_hasTrackMuon_2");
        tree->makeOutputVariable(m_muon_hasTrackJet_2, "muon_hasTrackJet_2");
        tree->makeOutputVariable(m_muon_energyLoss_2, "muon_energyLoss_2");
        tree->makeOutputVariable(m_muon_ptFrac_2, "muon_ptFrac_2");
        tree->makeOutputVariable(m_muon_ptRaw_2, "muon_ptRaw_2");
        tree->makeOutputVariable(m_muon_ptvarcone30_2, "muon_ptvarcone30_2");
        tree->makeOutputVariable(m_muon_ptvarcone30Rel_2, "muon_ptvarcone30Rel_2");
        tree->makeOutputVariable(m_muon_topoetcone30_2, "muon_topoetcone30_2");
        tree->makeOutputVariable(m_muon_topoetcone30Rel_2, "muon_topoetcone30Rel_2");
        tree->makeOutputVariable(m_muon_topoetcone40_2, "muon_topoetcone40_2");
        tree->makeOutputVariable(m_muon_topoetcone40Rel_2, "muon_topoetcone40Rel_2");
        // muon track
        tree->makeOutputVariable(m_muon_trackMuPt_2, "muon_trackMuPt_2");
        tree->makeOutputVariable(m_muon_trackMuEta_2, "muon_trackMuEta_2");
        tree->makeOutputVariable(m_muon_trackMuPhi_2, "muon_trackMuPhi_2");
        tree->makeOutputVariable(m_muon_trackMuE_2, "muon_trackMuE_2");
        tree->makeOutputVariable(m_muon_trackMuD0_2, "muon_trackMuD0_2");
        tree->makeOutputVariable(m_muon_trackMuD0Sig_2, "muon_trackMuD0Sig_2");
        tree->makeOutputVariable(m_muon_trackMuNPixelHits_2, "muon_trackMuNPixelHits_2");
        tree->makeOutputVariable(m_muon_trackMuNPixelHoles_2, "muon_trackMuNPixelHoles_2");
        tree->makeOutputVariable(m_muon_trackMuNPixelSharedHits_2, "muon_trackMuNPixelSharedHits_2");
        tree->makeOutputVariable(m_muon_trackMuNSCTHits_2, "muon_trackMuNSCTHits_2");
        tree->makeOutputVariable(m_muon_trackMuNSCTHoles_2, "muon_trackMuNSCTHoles_2");
        tree->makeOutputVariable(m_muon_trackMuNSCTSharedHits_2, "muon_trackMuNSCTSharedHits_2");
        tree->makeOutputVariable(m_muon_trackMuNSiHits_2, "muon_trackMuNSiHits_2");
        tree->makeOutputVariable(m_muon_trackMuNSiHoles_2, "muon_trackMuNSiHoles_2");
        tree->makeOutputVariable(m_muon_trackMuNSiSharedHits_2, "muon_trackMuNSiSharedHits_2");
        tree->makeOutputVariable(m_muon_trackMuZ0_2, "muon_trackMuZ0_2");
        tree->makeOutputVariable(m_muon_trackMuZ0Sig_2, "muon_trackMuZ0Sig_2");
        tree->makeOutputVariable(m_muon_trackMuZ0Sin_2, "muon_trackMuZ0Sin_2");
        // trackjet matched to muon track
        tree->makeOutputVariable(m_muon_trackMuonHasTrackJet_2, "muon_trackMuonHasTrackJet_2");
        tree->makeOutputVariable(m_muon_trackMuDrTrackJet_2, "muon_trackMuDrTrackJet_2");
        tree->makeOutputVariable(m_muon_trackMuPtRelTrackJet_2, "muon_trackMuPtRelTrackJet_2");
        tree->makeOutputVariable(m_muon_trackJetPt_2, "muon_trackJetPt_2");
        tree->makeOutputVariable(m_muon_trackJetEta_2, "muon_trackJetEta_2");
        tree->makeOutputVariable(m_muon_trackJetPhi_2, "muon_trackJetPhi_2");
        tree->makeOutputVariable(m_muon_trackJetE_2, "muon_trackJetE_2");
        
        tree->makeOutputVariable(m_muon_Pt_3, "muon_Pt_3");
        tree->makeOutputVariable(m_muon_Eta_3, "muon_Eta_3");
        tree->makeOutputVariable(m_muon_Phi_3, "muon_Phi_3");
        tree->makeOutputVariable(m_muon_E_3, "muon_E_3");
        tree->makeOutputVariable(m_muon_ID_3, "muon_ID_3");
        tree->makeOutputVariable(m_muon_IFF_3, "muon_IFF_3");
        tree->makeOutputVariable(m_muon_isPrompt_3, "muon_isPrompt_3");
        tree->makeOutputVariable(m_muon_calE_3, "muon_calE_3");
        tree->makeOutputVariable(m_muon_calERel_3, "muon_calERel_3");
        tree->makeOutputVariable(m_muon_dRlj_3, "muon_dRlj_3");
        tree->makeOutputVariable(m_muon_dRljAll_3, "muon_dRljAll_3");
        tree->makeOutputVariable(m_muon_hasTrackMuon_3, "muon_hasTrackMuon_3");
        tree->makeOutputVariable(m_muon_hasTrackJet_3, "muon_hasTrackJet_3");
        tree->makeOutputVariable(m_muon_energyLoss_3, "muon_energyLoss_3");
        tree->makeOutputVariable(m_muon_ptFrac_3, "muon_ptFrac_3");
        tree->makeOutputVariable(m_muon_ptRaw_3, "muon_ptRaw_3");
        tree->makeOutputVariable(m_muon_ptvarcone30_3, "muon_ptvarcone30_3");
        tree->makeOutputVariable(m_muon_ptvarcone30Rel_3, "muon_ptvarcone30Rel_3");
        tree->makeOutputVariable(m_muon_topoetcone30_3, "muon_topoetcone30_3");
        tree->makeOutputVariable(m_muon_topoetcone30Rel_3, "muon_topoetcone30Rel_3");
        tree->makeOutputVariable(m_muon_topoetcone40_3, "muon_topoetcone40_3");
        tree->makeOutputVariable(m_muon_topoetcone40Rel_3, "muon_topoetcone40Rel_3");
        // muon track
        tree->makeOutputVariable(m_muon_trackMuPt_3, "muon_trackMuPt_3");
        tree->makeOutputVariable(m_muon_trackMuEta_3, "muon_trackMuEta_3");
        tree->makeOutputVariable(m_muon_trackMuPhi_3, "muon_trackMuPhi_3");
        tree->makeOutputVariable(m_muon_trackMuE_3, "muon_trackMuE_3");
        tree->makeOutputVariable(m_muon_trackMuD0_3, "muon_trackMuD0_3");
        tree->makeOutputVariable(m_muon_trackMuD0Sig_3, "muon_trackMuD0Sig_3");
        tree->makeOutputVariable(m_muon_trackMuNPixelHits_3, "muon_trackMuNPixelHits_3");
        tree->makeOutputVariable(m_muon_trackMuNPixelHoles_3, "muon_trackMuNPixelHoles_3");
        tree->makeOutputVariable(m_muon_trackMuNPixelSharedHits_3, "muon_trackMuNPixelSharedHits_3");
        tree->makeOutputVariable(m_muon_trackMuNSCTHits_3, "muon_trackMuNSCTHits_3");
        tree->makeOutputVariable(m_muon_trackMuNSCTHoles_3, "muon_trackMuNSCTHoles_3");
        tree->makeOutputVariable(m_muon_trackMuNSCTSharedHits_3, "muon_trackMuNSCTSharedHits_3");
        tree->makeOutputVariable(m_muon_trackMuNSiHits_3, "muon_trackMuNSiHits_3");
        tree->makeOutputVariable(m_muon_trackMuNSiHoles_3, "muon_trackMuNSiHoles_3");
        tree->makeOutputVariable(m_muon_trackMuNSiSharedHits_3, "muon_trackMuNSiSharedHits_3");
        tree->makeOutputVariable(m_muon_trackMuZ0_3, "muon_trackMuZ0_3");
        tree->makeOutputVariable(m_muon_trackMuZ0Sig_3, "muon_trackMuZ0Sig_3");
        tree->makeOutputVariable(m_muon_trackMuZ0Sin_3, "muon_trackMuZ0Sin_3");
        // trackjet matched to muon track
        tree->makeOutputVariable(m_muon_trackMuonHasTrackJet_3, "muon_trackMuonHasTrackJet_3");
        tree->makeOutputVariable(m_muon_trackMuDrTrackJet_3, "muon_trackMuDrTrackJet_3");
        tree->makeOutputVariable(m_muon_trackMuPtRelTrackJet_3, "muon_trackMuPtRelTrackJet_3");
        tree->makeOutputVariable(m_muon_trackJetPt_3, "muon_trackJetPt_3");
        tree->makeOutputVariable(m_muon_trackJetEta_3, "muon_trackJetEta_3");
        tree->makeOutputVariable(m_muon_trackJetPhi_3, "muon_trackJetPhi_3");
        tree->makeOutputVariable(m_muon_trackJetE_3, "muon_trackJetE_3");
    }

    bool DecorateLeptons::checkTrackMuon(const xAOD::TrackParticle *trackLep)
    {
        if (!trackLep)
            return false;

        uint8_t numberOfPixelHits = 0;
        uint8_t numberOfSCTHits = 0;
        uint8_t numberOfPixelHoles = 0;
        uint8_t numberOfSCTHoles = 0;
        uint8_t numberOfPixelSharedHits = 0;
        uint8_t numberOfSCTSharedHits = 0;

        if (!(trackLep->summaryValue(numberOfPixelHits, xAOD::numberOfPixelHits)))
        return false;
        if (!(trackLep->summaryValue(numberOfSCTHits, xAOD::numberOfSCTHits)))
        return false;
        if (!(trackLep->summaryValue(numberOfPixelHoles, xAOD::numberOfPixelHoles)))
        return false;
        if (!(trackLep->summaryValue(numberOfSCTHoles, xAOD::numberOfSCTHoles)))
        return false;
        if (!(trackLep->summaryValue(numberOfPixelSharedHits, xAOD::numberOfPixelSharedHits)))
        return false;
        if (!(trackLep->summaryValue(numberOfSCTSharedHits, xAOD::numberOfSCTSharedHits)))
        return false;
        
        return true;
    }

    const xAOD::TrackParticle* DecorateLeptons::findTrackMuon(const xAOD::Muon* muon)
    {
        if (muon->muonType() != xAOD::Muon::Combined || !muon->inDetTrackParticleLink().isValid())
        {
        return nullptr;
        }
        const xAOD::TrackParticle *trackMuon = *(muon->inDetTrackParticleLink());
        return trackMuon;
    }

}
