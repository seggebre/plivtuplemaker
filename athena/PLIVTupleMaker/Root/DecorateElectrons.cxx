#include "PLIVTupleMaker/DecorateElectrons.h"
#include "PLIVTupleMaker/PLIVUtils.h"

#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h"
#include "TopEventSelectionTools/TreeManager.h"

#include "xAODEgamma/EgammaxAODHelpers.h"
#include "xAODCaloEvent/CaloClusterContainer.h"

#include "TMath.h"

#include <vector>

namespace top{
  ///-- Constructor --///
 DecorateElectrons::DecorateElectrons():
    m_maxLepTrackJetDr(0.4),
    m_caloClusterContainerName("egammaClusters"),
    m_elecMinCalERelConeSize(0.15)
  {
  }

  ///-- saveEvent - run for every systematic and every event --///
  void DecorateElectrons::ProcessEvent(const top::Event& event, ToolHandle<CP::IClassificationTool>& iffTool){

    const xAOD::CaloClusterContainer *clusters = nullptr;
    top::check(evtStore()->retrieve(clusters, m_caloClusterContainerName), "Failed to retrieve CaloClusterContainer");

    m_el_calELarge.clear();
    m_el_calELargeRel.clear();
    m_el_dRlj.clear();
    m_el_iffClass.clear();
    m_el_hasTrackElec.clear();
    m_el_ptFrac.clear();
    m_el_ptRel.clear();
    m_el_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt500.clear();
    m_el_ptvarcone30Rel.clear();
    m_el_topoetcone40.clear();
    m_el_topoetcone40Rel.clear();
    m_el_trackJetNTracks.clear();

    m_trackEl_pt.clear();
    m_trackEl_eta.clear();
    m_trackEl_phi.clear();
    m_trackEl_e.clear();

    for (auto elec : event.m_electrons){
      float calEta = -99.;
      float calPhi = -99.;
      float dRlj = -99.;
      float elecPt = -99.;
      int hasTrackElec = 0;
      unsigned int iffClass = 0;
      float ptFrac = -99.;
      float ptRel = -99.;
      float ptvarcone30 = -99.;
      float ptvarcone30Rel = -99.;
      float sumCoreEtLarge = 0.;
      float sumCoreEtLargeRel = 0.;
      float topoetcone40 = -99.;
      float topoetcone40Rel = -99.;
      size_t trackJetNTracks = 0; 
      top::check(iffTool->classify(*elec, iffClass), "Fail to get IFF class of this electron");
      elecPt = elec->pt();
      if (elec->isAvailable<float>("ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt500"))
      {
        ptvarcone30 = elec->auxdata<float>("ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt500");
        ptvarcone30Rel = ptvarcone30 / elecPt;
      }
      if (elec->isolation(topoetcone40, xAOD::Iso::topoetcone40))
        topoetcone40Rel = topoetcone40 / elecPt;
      m_el_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt500.push_back(ptvarcone30);
      m_el_ptvarcone30Rel.push_back(ptvarcone30Rel);
      m_el_iffClass.push_back(iffClass);
      m_el_topoetcone40.push_back(topoetcone40);
      m_el_topoetcone40Rel.push_back(topoetcone40Rel);

      // input variables in the EM cluster

      if (elec->caloCluster())
      {
        calEta = elec->caloCluster()->eta();
        calPhi = elec->caloCluster()->phi();
        for (auto *cluster : *clusters)
        {
          const float dEta = calEta - cluster->eta();
          const float dPhi = calPhi - cluster->phi();
          const float dR = TMath::Sqrt(dEta * dEta + dPhi * dPhi);
          if (dR < m_elecMinCalERelConeSize)
            sumCoreEtLarge += cluster->pt();
        }
      }
      if (elecPt > 0 && sumCoreEtLarge > 0)
        sumCoreEtLargeRel = sumCoreEtLarge / elecPt;
      m_el_calELarge.push_back(sumCoreEtLarge);
      m_el_calELargeRel.push_back(sumCoreEtLargeRel);

      // match track jet and store BDT inputs related to the track jet

      auto closestTrackJet = top::PLIV::findClosestJet(*elec, event.m_trackJets, m_maxLepTrackJetDr);
      if (closestTrackJet)
      {
        ptFrac = elec->trackParticle()->pt() / closestTrackJet->pt();
        const float angle = elec->p4().Vect().Angle(closestTrackJet->p4().Vect());
        ptRel = elecPt * TMath::Sin(angle);
        dRlj = closestTrackJet->p4().DeltaR(elec->p4());
        trackJetNTracks = closestTrackJet->getConstituents().size();
      }
      m_el_dRlj.push_back(dRlj);
      m_el_ptFrac.push_back(ptFrac);
      m_el_ptRel.push_back(ptRel);
      m_el_trackJetNTracks.push_back(trackJetNTracks);

      // find and store track electron information

      float trackElecPt = -99.;
      float trackElecEta = -99.;
      float trackElecPhi = -99.;
      float trackElecE = -99.;

      auto trackElec = findTrackElec(elec);

      if (trackElec && checkTrackElec(trackElec))
      {
        hasTrackElec = 1;
        trackElecPt = trackElec->pt();
        trackElecEta = trackElec->eta();
        trackElecPhi = trackElec->phi();
        trackElecE = trackElec->e();
      }

      m_el_hasTrackElec.push_back(hasTrackElec);
      m_trackEl_pt.push_back(trackElecPt);
      m_trackEl_eta.push_back(trackElecEta);
      m_trackEl_phi.push_back(trackElecPhi);
      m_trackEl_e.push_back(trackElecE);
    }
  }

  void DecorateElectrons::SetBranches(std::shared_ptr<top::TreeManager> tree){
    tree->makeOutputVariable(m_el_calELarge, "el_calELarge");
    tree->makeOutputVariable(m_el_calELargeRel, "el_calELargeRel");
    tree->makeOutputVariable(m_el_dRlj, "el_dRlj");
    tree->makeOutputVariable(m_el_iffClass, "el_iffClass");
    tree->makeOutputVariable(m_el_hasTrackElec, "el_hasTrackElec");
    tree->makeOutputVariable(m_el_ptFrac, "el_ptFrac");
    tree->makeOutputVariable(m_el_ptRel, "el_ptRel");
    tree->makeOutputVariable(m_el_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt500, "el_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt500");
    tree->makeOutputVariable(m_el_ptvarcone30Rel, "el_ptvarcone30Rel");
    tree->makeOutputVariable(m_el_topoetcone40, "el_topoetcone40");
    tree->makeOutputVariable(m_el_topoetcone40Rel, "el_topoetcone40Rel");
    tree->makeOutputVariable(m_el_trackJetNTracks, "el_trackJetNTracks");
    tree->makeOutputVariable(m_trackEl_pt, "trackEl_pt");
    tree->makeOutputVariable(m_trackEl_eta, "trackEl_eta");
    tree->makeOutputVariable(m_trackEl_phi, "trackEl_phi");
    tree->makeOutputVariable(m_trackEl_e, "trackEl_e");
  }

  bool DecorateElectrons::checkTrackElec(const xAOD::TrackParticle *trackLep){
    // check the track muon is good
    if (!trackLep) return false;

    uint8_t numberOfPixelHits = 0;
    uint8_t numberOfSCTHits = 0;
    uint8_t numberOfPixelHoles = 0;
    uint8_t numberOfSCTHoles = 0;
    uint8_t numberOfPixelSharedHits = 0;
    uint8_t numberOfSCTSharedHits = 0;

    if (!(trackLep->summaryValue(numberOfPixelHits, xAOD::numberOfPixelHits))) return false;
    if (!(trackLep->summaryValue(numberOfSCTHits, xAOD::numberOfSCTHits))) return false;
    if (!(trackLep->summaryValue(numberOfPixelHoles, xAOD::numberOfPixelHoles))) return false;
    if (!(trackLep->summaryValue(numberOfSCTHoles, xAOD::numberOfSCTHoles))) return false;
    if (!(trackLep->summaryValue(numberOfPixelSharedHits, xAOD::numberOfPixelSharedHits))) return false;
    if (!(trackLep->summaryValue(numberOfSCTSharedHits, xAOD::numberOfSCTSharedHits))) return false;
    
    return true;
  }

  const xAOD::TrackParticle* DecorateElectrons::findTrackElec(const xAOD::Electron* elec){
    const xAOD::TrackParticle *trackElec = nullptr;
    const xAOD::TrackParticle *bestMatchedGSFElTrack = elec->trackParticle(0);
    if (bestMatchedGSFElTrack)
    {
      trackElec = xAOD::EgammaHelpers::getOriginalTrackParticleFromGSF(bestMatchedGSFElTrack);
    }
    return trackElec;
  }
}
