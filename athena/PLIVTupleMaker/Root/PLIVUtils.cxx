#include "PLIVTupleMaker/PLIVUtils.h"
#include "xAODTracking/Vertex.h"

const xAOD::Vertex* top::PLIV::findPrimaryVertex(const xAOD::VertexContainer *vertexContainer)
{
	const xAOD::Vertex *priVertex = nullptr;
	for (auto *vertex : *vertexContainer)
    {
      if (vertex->vertexType() == 1)
      {
        priVertex = vertex;
        break;
      }
    }
	return priVertex;
}

const xAOD::Vertex* top::PLIV::findSecondaryVertex(const xAOD::VertexContainer *vertexContainer)
{
	const xAOD::Vertex *secVertex = nullptr;
	for (auto *vertex : *vertexContainer)
    {
      
      if (vertex->vertexType() == 2)
      {
        std::cout << "Vertex Type: " << vertex->vertexType() <<std::endl;
        secVertex = vertex;
        break;
      }
    }
	return secVertex;
}