/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "PLIVTupleMaker/PLIVObjectLoader.h"
#include "PLIVTupleMaker/DecorateElectrons.h"
#include "PLIVTupleMaker/DecorateMuons.h"
#include "PLIVTupleMaker/DecorateTracks.h"
#include "PLIVTupleMaker/DecorateLeptons.h"
#include "PLIVTupleMaker/DummyOverlapRemoval.h"
#include "PLIVTupleMaker/PLIVEventSaver.h"
#include "PLIVTupleMaker/PLIVTupleMakerLoader.h"

#ifdef __CINT__

#pragma extra_include "PLIVTupleMaker/PLIVObjectLoader.h";
#pragma extra_include "PLIVTupleMaker/DecorateElectrons.h";
#pragma extra_include "PLIVTupleMaker/DecorateMuons.h";
#pragma extra_include "PLIVTupleMaker/DecorateTracks.h";
#pragma extra_include "PLIVTupleMaker/DecorateLeptons.h";
#pragma extra_include "PLIVTupleMaker/DummyOverlapRemoval.h";
#pragma extra_include "PLIVTupleMaker/PLIVEventSaver.h";
#pragma extra_include "PLIVTupleMaker/PLIVTupleMakerLoader.h";

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

//for loading the object selection at run time
#pragma link C++ class top::PLIVObjectLoader+;
#pragma link C++ class top::DecorateElectrons+;
#pragma link C++ class top::DecorateMuons+;
#pragma link C++ class top::DecorateTracks+;
#pragma link C++ class top::PLIVEventSaver+;
#pragma link C++ class top::PLIVTupleMakerLoader+;

#endif
