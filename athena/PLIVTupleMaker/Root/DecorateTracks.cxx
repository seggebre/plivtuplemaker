#include "PLIVTupleMaker/DecorateTracks.h"
#include "PLIVTupleMaker/PLIVUtils.h"

#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h"
#include "TopEventSelectionTools/TreeManager.h"

#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODEgamma/EgammaxAODHelpers.h"

#include "TMath.h"

#include <vector>

namespace top{
    DecorateTracks::DecorateTracks() :
    m_storeTrackVars(false),
    m_trackContainerName("InDetTrackParticles"),
    m_maxLepTrackJetDr(1.0)
  {
    m_configSettings = top::ConfigurationSettings::get();
    if (m_configSettings->retrieve("TestInDetTrackSaver"))
    {
      const std::string &keyReadTrack = m_configSettings->value("TestInDetTrackSaver");
      if (keyReadTrack == "True")
      {
        m_storeTrackVars = true;
      }
      else if (keyReadTrack == "False")
      {
        m_storeTrackVars = false;
      }
      else
      {
        throw std::invalid_argument{"Please provide \'True\' or \'False\' for \'TestInDetTrackSaver\' dynamic key"};
      }
    }
  }

  void DecorateTracks::ProcessEvent(const top::Event &event)
  {
    //std::string m_secondaryVertexKey;
    //const xAOD::VertexContainer *secondaryVtxCollection = nullptr;
    //declareProperty("SecondaryVertexCollection", m_secondaryVertexKey = "SecVertices", "Vertices to use as secondary vertex");
    //top::check(evtStore()->retrieve(secondaryVtxCollection, m_secondaryVertexKey), "Fail to retrieve secondary verticies.");

    const xAOD::TrackParticleContainer *tracks = nullptr;
    //const xAOD::Vertex *secVertex = PLIV::findSecondaryVertex(secondaryVtxCollection);
    const xAOD::Vertex *priVertex = PLIV::findPrimaryVertex(event.m_primaryVertices);
    top::check(evtStore()->retrieve(tracks, m_trackContainerName), "Fail to retrieve the track container");

    // find primary vertex
    readTrackInfo(tracks, priVertex);
  }

  void DecorateTracks::readTrackInfo(const xAOD::TrackParticleContainer *tracks, const xAOD::Vertex *priVertex)
  {
    ///-- set our variables to zero --///
    m_track_d0.clear();
    m_track_d0Sig.clear();
    m_track_eta.clear();
    m_track_nPixelHits.clear();
    m_track_nPixelHoles.clear();
    m_track_nPixelSharedHits.clear();
    m_track_nSCTHits.clear();
    m_track_nSCTHoles.clear();
    m_track_nSCTSharedHits.clear();
    m_track_nSiHits.clear();
    m_track_nSiHoles.clear();
    m_track_nSiSharedHits.clear();
    m_track_phi.clear();
    m_track_pt.clear();
    m_track_z0.clear();
    m_track_z0Sin.clear();
    m_track_z0Sig.clear();
    
    for (const xAOD::TrackParticle *track : *tracks)
    {
      float d0 = -99.;
      float d0Sig = -99.;
      float eta = -99.;
      uint8_t nPixelHits = 0;
      uint8_t nPixelHoles = 0;
      uint8_t nPixelSharedHits = 0;
      uint8_t nSCTHits = 0;
      uint8_t nSCTHoles = 0;
      uint8_t nSCTSharedHits = 0;
      uint8_t nSiHits = 0; // nPixelHits + nSCTHits
      uint8_t nSiHoles = 0;
      float nSiSharedHits = 0; // nPixelSharedHits + nSCTSharedHits / 2
      float phi = -99.;
      float pt = -99.;
      float z0 = -99.;
      float z0Sig = -99.;
      float z0Sin = -99.;

      d0 = track->d0();
      d0Sig = xAOD::TrackingHelpers::d0significance(track);
      eta = track->eta();
      if (track->isAvailable<uint8_t>("numberOfPixelHits"))
        nPixelHits = track->auxdata<uint8_t>("numberOfPixelHits");
      if (track->isAvailable<uint8_t>("numberOfPixelHoles"))
        nPixelHoles = track->auxdata<uint8_t>("numberOfPixelHoles");
      if (track->isAvailable<uint8_t>("numberOfPixelSharedHits"))
        nPixelSharedHits = track->auxdata<uint8_t>("numberOfPixelSharedHits");
      if (track->isAvailable<uint8_t>("numberOfSCTHits"))
        nSCTHits = track->auxdata<uint8_t>("numberOfSCTHits");
      if (track->isAvailable<uint8_t>("numberOfSCTHoles"))
        nSCTHits = track->auxdata<uint8_t>("numberOfSCTHoles");
      if (track->isAvailable<uint8_t>("numberOfSCTSharedHits"))
        nSCTSharedHits = track->auxdata<uint8_t>("numberOfSCTSharedHits");
      nSiHits = nPixelHits + nSCTHits;
      nSiHoles = nPixelHoles + nSCTHoles;
      nSiSharedHits = float(nPixelSharedHits) + float(nSCTSharedHits) / 2;
      phi = track->phi();
      pt = track->pt();
      z0 = track->z0();
      z0Sig = xAOD::TrackingHelpers::z0significance(track);
      if (priVertex)
      {
        const float deltaZ0 = track->z0() + track->vz() - priVertex->z();
        z0Sin = deltaZ0 * TMath::Sin(track->theta());
      }
      if (!(checkTrack(pt, eta, z0Sin, nSiHits, nSiHoles, nSiSharedHits, nPixelHoles)))
        continue;
      m_track_d0.push_back(d0);
      m_track_d0Sig.push_back(d0Sig);
      m_track_eta.push_back(eta);
      m_track_nPixelHits.push_back(nPixelHits);
      m_track_nPixelHoles.push_back(nPixelHoles);
      m_track_nPixelSharedHits.push_back(nPixelSharedHits);
      m_track_nSCTHits.push_back(nSCTHits);
      m_track_nSCTHoles.push_back(nSCTHoles);
      m_track_nSCTSharedHits.push_back(nSCTSharedHits);
      m_track_nSiHits.push_back(nSiHits);
      m_track_nSiHoles.push_back(nSiHoles);
      m_track_nSiSharedHits.push_back(nSiSharedHits);
      m_track_phi.push_back(phi);
      m_track_pt.push_back(pt);
      m_track_z0.push_back(z0);
      m_track_z0Sig.push_back(z0Sig);
      m_track_z0Sin.push_back(z0Sin);
    }
  }

  void DecorateTracks::SetBranches(std::shared_ptr<top::TreeManager> tree)
  {
    tree->makeOutputVariable(m_track_d0, "idTrack_d0");
    tree->makeOutputVariable(m_track_d0Sig, "idTrack_d0Sig");
    tree->makeOutputVariable(m_track_eta, "idTrack_eta");
    tree->makeOutputVariable(m_track_nPixelHits, "idTrack_nPixelHits");
    tree->makeOutputVariable(m_track_nPixelHoles, "idTrack_nPixelHoles");
    tree->makeOutputVariable(m_track_nPixelSharedHits, "idTrack_nPixelSharedHits");
    tree->makeOutputVariable(m_track_nSCTHits, "idTrack_nSCTHits");
    tree->makeOutputVariable(m_track_nSCTHoles, "idTrack_nSCTHoles");
    tree->makeOutputVariable(m_track_nSCTSharedHits, "idTrack_nSCTSharedHits");
    tree->makeOutputVariable(m_track_nSiHits, "idTrack_nSiHits");
    tree->makeOutputVariable(m_track_nSiHoles, "idTrack_nSiHoles");
    tree->makeOutputVariable(m_track_phi, "idTrack_phi");
    tree->makeOutputVariable(m_track_pt, "idTrack_pt");
    tree->makeOutputVariable(m_track_z0, "idTrack_z0");
    tree->makeOutputVariable(m_track_z0Sig, "idTrack_z0Sig");
    tree->makeOutputVariable(m_track_z0Sin, "idTrack_z0Sin");
  }

  bool DecorateTracks::checkTrackLep(const xAOD::TrackParticle *trackLep)
  {
    uint8_t numberOfPixelHits = 0;
    uint8_t numberOfSCTHits = 0;
    uint8_t numberOfPixelHoles = 0;
    uint8_t numberOfSCTHoles = 0;
    uint8_t numberOfPixelSharedHits = 0;
    uint8_t numberOfSCTSharedHits = 0;

    if (!(trackLep->summaryValue(numberOfPixelHits, xAOD::numberOfPixelHits)))
      return false;
    if (!(trackLep->summaryValue(numberOfSCTHits, xAOD::numberOfSCTHits)))
      return false;
    if (!(trackLep->summaryValue(numberOfPixelHoles, xAOD::numberOfPixelHoles)))
      return false;
    if (!(trackLep->summaryValue(numberOfSCTHoles, xAOD::numberOfSCTHoles)))
      return false;
    if (!(trackLep->summaryValue(numberOfPixelSharedHits, xAOD::numberOfPixelSharedHits)))
      return false;
    if (!(trackLep->summaryValue(numberOfSCTSharedHits, xAOD::numberOfSCTSharedHits)))
      return false;
    
    return true;
  }

  bool DecorateTracks::checkTrack(const float &trackPt, const float &trackEta, const float &z0Sin, const float &nSiHits, const float &nSiHoles, const float &nSiSharedHits, const float &nPixelHoles)
  {
    if (!(trackPt > 500))
      return false;
    if (!(TMath::Abs(trackEta) < 2.5))
      return false;
    if (!(TMath::Abs(z0Sin) < 1))
      return false;
    if (!(nSiHits > 6.5))
      return false;
    if (!(nSiSharedHits < 1.5))
      return false;
    if (!(nSiHoles < 2.5))
      return false;
    if (!(nPixelHoles < 1.5))
      return false;
    return true;
  }
}
