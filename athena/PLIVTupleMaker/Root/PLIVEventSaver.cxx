#include "PLIVTupleMaker/PLIVEventSaver.h"
#include "PLIVTupleMaker/DecorateTracks.h"
#include "PLIVTupleMaker/PLIVUtils.h"

#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h"
#include "TopEventSelectionTools/TreeManager.h"
#include "TopConfiguration/TopConfig.h"
#include "AsgTools/AnaToolHandle.h"

#include "TruthClassification/TruthClassificationTool.h"

namespace top{
  PLIVEventSaver::PLIVEventSaver(){
  }
  
  void PLIVEventSaver::initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches){
    top::EventSaverFlatNtuple::initialize(config, file, extraBranches);

    asg::AsgToolConfig toolConfig("TruthClassificationTool");
    ANA_MSG_INFO("Start to set the IFF TruthClassification tool");
    top::check(toolConfig.setProperty("separateChargeFlipElectrons", true), "Failed to set separateChargeFlipElectrons");
    top::check(toolConfig.setProperty("separateChargeFlipMuons",     true), "Failed to set separateChargeFlipMuons");
    top::check(toolConfig.makePrivateTool(m_truthTool), "Failed to set TruthClassification tool");
    top::check(m_truthTool.retrieve(), "Print this message if retrieving the tool fails");

    for (auto systematicTree : treeManagers()) {
      m_TrackDecorater.SetBranches(systematicTree);
      m_LeptonDecorater.SetBranches(systematicTree);
      m_MuonDecorater.SetBranches(systematicTree);
      m_ElectronDecorater.SetBranches(systematicTree);
    }
  }

  void PLIVEventSaver::calculateEvent(const top::Event& event) {
    top::EventSaverFlatNtuple::calculateEvent(event);

    m_TrackDecorater.ProcessEvent(event);
    m_LeptonDecorater.ProcessEvent(event, m_truthTool);
    m_MuonDecorater.ProcessEvent(event, m_truthTool);
    m_ElectronDecorater.ProcessEvent(event, m_truthTool);

  }

}
