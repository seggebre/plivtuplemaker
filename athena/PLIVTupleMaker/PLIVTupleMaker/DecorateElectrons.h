#ifndef DECORATEELECTRONS_H
#define DECORATEELECTRONS_H

#include "TopAnalysis/EventSaverFlatNtuple.h"
#include "TopConfiguration/ConfigurationSettings.h"
#include "TopEventSelectionTools/TreeManager.h"

#include "AsgTools/AsgTool.h"
#include "AsgTools/ToolHandle.h"
#include "AsgTools/AnaToolHandle.h"
#include "TruthClassification/TruthClassificationTool.h"

#include "PLIVTupleMaker/PLIVUtils.h"

#include "TMath.h"
#include <vector>

namespace top{

    class DecorateElectrons : public top::EventSaverFlatNtuple{
        public:
            DecorateElectrons();
            virtual ~DecorateElectrons() = default;

            virtual void ProcessEvent(const top::Event& event, ToolHandle<CP::IClassificationTool>& iffTool);
            void SetBranches(std::shared_ptr<top::TreeManager> tree);
            const xAOD::TrackParticle* findTrackElec(const xAOD::Electron *elec);
            bool checkTrackElec(const xAOD::TrackParticle *trackLep);

        private:
            std::vector<float> m_el_calELarge;
            std::vector<float> m_el_calELargeRel;
            std::vector<float> m_el_dRlj;
            std::vector<unsigned int> m_el_iffClass;
            std::vector<int> m_el_hasTrackElec;
            std::vector<float> m_el_ptFrac;
            std::vector<float> m_el_ptRel;
            std::vector<float> m_el_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt500;
            std::vector<float> m_el_ptvarcone30Rel;
            std::vector<float> m_el_topoetcone40;
            std::vector<float> m_el_topoetcone40Rel;
            std::vector<float> m_el_trackJetNTracks;

            std::vector<float> m_trackEl_pt;
            std::vector<float> m_trackEl_eta;
            std::vector<float> m_trackEl_phi;
            std::vector<float> m_trackEl_e;

            const float m_maxLepTrackJetDr;
            const std::string m_caloClusterContainerName;
            const float m_elecMinCalERelConeSize;

    };
}

#endif
