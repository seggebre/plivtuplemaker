#ifndef PLIVOBJECTLOADER_H_
#define PLIVOBJECTLOADER_H_

#include "TopAnalysis/ObjectLoaderBase.h"

/**
 * A class that can be loaded by name at run time and creates our object selection
 */

namespace top{

    class PLIVObjectLoader : public ObjectLoaderBase{
        public:
            top::TopObjectSelection* init(std::shared_ptr<top::TopConfig> TopConfig);

    };
}

#endif