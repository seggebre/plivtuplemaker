#ifndef DECORATETRACKS_H
#define DECORATETRACKS_H

#include "TopAnalysis/EventSaverFlatNtuple.h"
#include "TopConfiguration/ConfigurationSettings.h"
#include "TopEventSelectionTools/TreeManager.h"

#include "PLIVTupleMaker/PLIVUtils.h"

#include <string>

namespace top{
  class DecorateTracks : public top::EventSaverFlatNtuple{
    public:
      DecorateTracks();
      virtual ~DecorateTracks() = default;

      virtual void ProcessEvent(const top::Event &event);
      void readTrackInfo(const xAOD::TrackParticleContainer *tracks, const xAOD::Vertex *priVertex);
      void SetBranches(std::shared_ptr<top::TreeManager> tree);
      bool checkTrackLep(const xAOD::TrackParticle *trackLep);
      bool checkTrack(const float &trackPt, const float &trackEta, const float &z0Sin, const float &nSiHits, const float &nSiHoles, const float &nSiSharedHits, const float &nPixelHoles);

    private:
      ConfigurationSettings* m_configSettings;

      bool m_storeTrackVars;

      const std::string m_trackContainerName;
      std::vector<float> m_track_d0;
      std::vector<float> m_track_d0Sig;
      std::vector<float> m_track_eta;
      std::vector<uint8_t> m_track_nPixelHits;
      std::vector<uint8_t> m_track_nPixelHoles;
      std::vector<uint8_t> m_track_nPixelSharedHits;
      std::vector<uint8_t> m_track_nSCTHits;
      std::vector<uint8_t> m_track_nSCTHoles;
      std::vector<uint8_t> m_track_nSCTSharedHits;
      std::vector<uint8_t> m_track_nSiHits;
      std::vector<uint8_t> m_track_nSiHoles;
      std::vector<float> m_track_nSiSharedHits;
      std::vector<float> m_track_phi;
      std::vector<float> m_track_pt;
      std::vector<float> m_track_z0;
      std::vector<float> m_track_z0Sig;
      std::vector<float> m_track_z0Sin;

      std::vector<float> m_track_dRlj;
      std::vector<float> m_track_ptFraclj;

      const float m_maxLepTrackJetDr;
  };
}

#endif
