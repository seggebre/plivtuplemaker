#ifndef PLIVUTILS_H
#define PLIVUTILS_H

#include "xAODJet/JetContainer.h"
#include "xAODTracking/VertexContainer.h"
#include <iostream>

namespace top
{
	namespace PLIV
	{
		template <typename T>
		const xAOD::Jet *findClosestJet(const T &particle, const xAOD::JetContainer &trackJets, const float &maxLepTrackJetDr)
		{
			const xAOD::Jet *closestTrackJet = nullptr;
			float lepTrackJetDr = -1.0;

			for (const xAOD::Jet *jet : trackJets)
			{
				float dr = particle.p4().DeltaR(jet->p4());
				if (lepTrackJetDr < 0.0 || dr < lepTrackJetDr)
				{
					closestTrackJet = jet;
					lepTrackJetDr = dr;
				}
			}

			if (closestTrackJet && lepTrackJetDr < maxLepTrackJetDr)
			{
				return closestTrackJet;
			}
			return nullptr;
		}

		const xAOD::Vertex* findPrimaryVertex(const xAOD::VertexContainer *vertexContainer);
		const xAOD::Vertex* findSecondaryVertex(const xAOD::VertexContainer *vertexContainer);
	}
}

#endif