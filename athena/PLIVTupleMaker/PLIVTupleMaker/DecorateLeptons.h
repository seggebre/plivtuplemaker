#ifndef DECORATELEPTONS_H
#define DECORATELEPTONS_H

#include "TopAnalysis/EventSaverFlatNtuple.h"
#include "TopConfiguration/ConfigurationSettings.h"
#include "TopEventSelectionTools/TreeManager.h"

#include "TruthClassification/TruthClassificationTool.h"

#include "AsgTools/AsgTool.h"
#include "AsgTools/ToolHandle.h"
#include "AsgTools/AnaToolHandle.h"

#include <string>
#include <vector>

namespace top{

    class DecorateLeptons : public top::EventSaverFlatNtuple{

        public:
            DecorateLeptons();
            virtual ~DecorateLeptons();

            virtual void ProcessEvent(const top::Event &event, ToolHandle<CP::IClassificationTool>& iffTool);
            void SetBranches(std::shared_ptr<top::TreeManager> tree);
            const xAOD::TrackParticle* findTrackMuon(const xAOD::Muon *muon);
            bool checkTrackMuon(const xAOD::TrackParticle *trackLep);        

        private:
            float m_maxLepTrackJetDr;
            float m_maxDeltaZ0Sin;
            float m_maxTopoEtCone30Rel;

            int m_total_leptons;
            int m_total_charge;
            int m_total_electrons;
            int m_total_muons;

            float m_electron_Pt_0;
            float m_electron_Eta_0;
            float m_electron_Phi_0;
            float m_electron_E_0;
            int m_electron_ID_0;
            int m_electron_IFF_0;
            int m_electron_isPrompt_0;

            float m_electron_Pt_1;
            float m_electron_Eta_1;
            float m_electron_Phi_1;
            float m_electron_E_1;
            int m_electron_ID_1;
            int m_electron_IFF_1;
            int m_electron_isPrompt_1;

            float m_electron_Pt_2;
            float m_electron_Eta_2;
            float m_electron_Phi_2;
            float m_electron_E_2;
            int m_electron_ID_2;
            int m_electron_IFF_2;
            int m_electron_isPrompt_2;

            float m_electron_Pt_3;
            float m_electron_Eta_3;
            float m_electron_Phi_3;
            float m_electron_E_3;
            int m_electron_ID_3;
            int m_electron_IFF_3;
            int m_electron_isPrompt_3;

            float m_muon_Pt_0;
            float m_muon_Eta_0;
            float m_muon_Phi_0;
            float m_muon_E_0;
            int m_muon_ID_0;
            int m_muon_IFF_0;
            int m_muon_isPrompt_0;
            float m_muon_calE_0;
            float m_muon_calERel_0;
            float m_muon_dRlj_0;
            float m_muon_dRljAll_0;
            int m_muon_hasTrackMuon_0;
            int m_muon_hasTrackJet_0;
            float m_muon_energyLoss_0;
            float m_muon_ptFrac_0;
            float m_muon_ptRaw_0;
            float m_muon_ptvarcone30_0;
            float m_muon_ptvarcone30Rel_0;
            float m_muon_topoetcone30_0;
            float m_muon_topoetcone30Rel_0;
            float m_muon_topoetcone40_0;
            float m_muon_topoetcone40Rel_0;
            // muon track
            float m_muon_trackMuPt_0;
            float m_muon_trackMuEta_0;
            float m_muon_trackMuPhi_0;
            float m_muon_trackMuE_0;
            float m_muon_trackMuD0_0;
            float m_muon_trackMuD0Sig_0;
            uint8_t m_muon_trackMuNPixelHits_0;
            uint8_t m_muon_trackMuNPixelHoles_0;
            uint8_t m_muon_trackMuNPixelSharedHits_0;
            uint8_t m_muon_trackMuNSCTHits_0;
            uint8_t m_muon_trackMuNSCTHoles_0;
            uint8_t m_muon_trackMuNSCTSharedHits_0;
            uint8_t m_muon_trackMuNSiHits_0;
            uint8_t m_muon_trackMuNSiHoles_0;
            uint8_t m_muon_trackMuNSiSharedHits_0;
            float m_muon_trackMuZ0_0;
            float m_muon_trackMuZ0Sig_0;
            float m_muon_trackMuZ0Sin_0;
            // trackjet matched to muon track
            int m_muon_trackMuonHasTrackJet_0;
            float m_muon_trackMuDrTrackJet_0;
            float m_muon_trackMuPtRelTrackJet_0;
            float m_muon_trackJetPt_0;
            float m_muon_trackJetEta_0;
            float m_muon_trackJetPhi_0;
            float m_muon_trackJetE_0;

            float m_muon_Pt_1;
            float m_muon_Eta_1;
            float m_muon_Phi_1;
            float m_muon_E_1;
            int m_muon_ID_1;
            int m_muon_IFF_1;
            int m_muon_isPrompt_1;
            float m_muon_calE_1;
            float m_muon_calERel_1;
            float m_muon_dRlj_1;
            float m_muon_dRljAll_1;
            int m_muon_hasTrackMuon_1;
            int m_muon_hasTrackJet_1;
            float m_muon_energyLoss_1;
            float m_muon_ptFrac_1;
            float m_muon_ptRaw_1;
            float m_muon_ptvarcone30_1;
            float m_muon_ptvarcone30Rel_1;
            float m_muon_topoetcone30_1;
            float m_muon_topoetcone30Rel_1;
            float m_muon_topoetcone40_1;
            float m_muon_topoetcone40Rel_1;
            // muon track
            float m_muon_trackMuPt_1;
            float m_muon_trackMuEta_1;
            float m_muon_trackMuPhi_1;
            float m_muon_trackMuE_1;
            float m_muon_trackMuD0_1;
            float m_muon_trackMuD0Sig_1;
            uint8_t m_muon_trackMuNPixelHits_1;
            uint8_t m_muon_trackMuNPixelHoles_1;
            uint8_t m_muon_trackMuNPixelSharedHits_1;
            uint8_t m_muon_trackMuNSCTHits_1;
            uint8_t m_muon_trackMuNSCTHoles_1;
            uint8_t m_muon_trackMuNSCTSharedHits_1;
            uint8_t m_muon_trackMuNSiHits_1;
            uint8_t m_muon_trackMuNSiHoles_1;
            uint8_t m_muon_trackMuNSiSharedHits_1;
            float m_muon_trackMuZ0_1;
            float m_muon_trackMuZ0Sig_1;
            float m_muon_trackMuZ0Sin_1;
            // trackjet matched to muon track
            int m_muon_trackMuonHasTrackJet_1;
            float m_muon_trackMuDrTrackJet_1;
            float m_muon_trackMuPtRelTrackJet_1;
            float m_muon_trackJetPt_1;
            float m_muon_trackJetEta_1;
            float m_muon_trackJetPhi_1;
            float m_muon_trackJetE_1;

            float m_muon_Pt_2;
            float m_muon_Eta_2;
            float m_muon_Phi_2;
            float m_muon_E_2;
            int m_muon_ID_2;
            int m_muon_IFF_2;
            int m_muon_isPrompt_2;
            float m_muon_calE_2;
            float m_muon_calERel_2;
            float m_muon_dRlj_2;
            float m_muon_dRljAll_2;
            int m_muon_hasTrackMuon_2;
            int m_muon_hasTrackJet_2;
            float m_muon_energyLoss_2;
            float m_muon_ptFrac_2;
            float m_muon_ptRaw_2;
            float m_muon_ptvarcone30_2;
            float m_muon_ptvarcone30Rel_2;
            float m_muon_topoetcone30_2;
            float m_muon_topoetcone30Rel_2;
            float m_muon_topoetcone40_2;
            float m_muon_topoetcone40Rel_2;
            // muon track
            float m_muon_trackMuPt_2;
            float m_muon_trackMuEta_2;
            float m_muon_trackMuPhi_2;
            float m_muon_trackMuE_2;
            float m_muon_trackMuD0_2;
            float m_muon_trackMuD0Sig_2;
            uint8_t m_muon_trackMuNPixelHits_2;
            uint8_t m_muon_trackMuNPixelHoles_2;
            uint8_t m_muon_trackMuNPixelSharedHits_2;
            uint8_t m_muon_trackMuNSCTHits_2;
            uint8_t m_muon_trackMuNSCTHoles_2;
            uint8_t m_muon_trackMuNSCTSharedHits_2;
            uint8_t m_muon_trackMuNSiHits_2;
            uint8_t m_muon_trackMuNSiHoles_2;
            uint8_t m_muon_trackMuNSiSharedHits_2;
            float m_muon_trackMuZ0_2;
            float m_muon_trackMuZ0Sig_2;
            float m_muon_trackMuZ0Sin_2;
            // trackjet matched to muon track
            int m_muon_trackMuonHasTrackJet_2;
            float m_muon_trackMuDrTrackJet_2;
            float m_muon_trackMuPtRelTrackJet_2;
            float m_muon_trackJetPt_2;
            float m_muon_trackJetEta_2;
            float m_muon_trackJetPhi_2;
            float m_muon_trackJetE_2;

            float m_muon_Pt_3;
            float m_muon_Eta_3;
            float m_muon_Phi_3;
            float m_muon_E_3;
            int m_muon_ID_3;
            int m_muon_IFF_3;
            int m_muon_isPrompt_3;
            float m_muon_calE_3;
            float m_muon_calERel_3;
            float m_muon_dRlj_3;
            float m_muon_dRljAll_3;
            int m_muon_hasTrackMuon_3;
            int m_muon_hasTrackJet_3;
            float m_muon_energyLoss_3;
            float m_muon_ptFrac_3;
            float m_muon_ptRaw_3;
            float m_muon_ptvarcone30_3;
            float m_muon_ptvarcone30Rel_3;
            float m_muon_topoetcone30_3;
            float m_muon_topoetcone30Rel_3;
            float m_muon_topoetcone40_3;
            float m_muon_topoetcone40Rel_3;
            // muon track
            float m_muon_trackMuPt_3;
            float m_muon_trackMuEta_3;
            float m_muon_trackMuPhi_3;
            float m_muon_trackMuE_3;
            float m_muon_trackMuD0_3;
            float m_muon_trackMuD0Sig_3;
            uint8_t m_muon_trackMuNPixelHits_3;
            uint8_t m_muon_trackMuNPixelHoles_3;
            uint8_t m_muon_trackMuNPixelSharedHits_3;
            uint8_t m_muon_trackMuNSCTHits_3;
            uint8_t m_muon_trackMuNSCTHoles_3;
            uint8_t m_muon_trackMuNSCTSharedHits_3;
            uint8_t m_muon_trackMuNSiHits_3;
            uint8_t m_muon_trackMuNSiHoles_3;
            uint8_t m_muon_trackMuNSiSharedHits_3;
            float m_muon_trackMuZ0_3;
            float m_muon_trackMuZ0Sig_3;
            float m_muon_trackMuZ0Sin_3;
            // trackjet matched to muon track
            int m_muon_trackMuonHasTrackJet_3;
            float m_muon_trackMuDrTrackJet_3;
            float m_muon_trackMuPtRelTrackJet_3;
            float m_muon_trackJetPt_3;
            float m_muon_trackJetEta_3;
            float m_muon_trackJetPhi_3;
            float m_muon_trackJetE_3;            
    };
}

#endif