#ifndef PLIVNTUPLEMAKER_PLIVEVENTSAVER_H
#define PLIVNTUPLEMAKER_PLIVEVENTSAVER_H

#include "TopAnalysis/EventSaverFlatNtuple.h"
#include "TopConfiguration/ConfigurationSettings.h"

#include "AsgTools/AsgTool.h"
#include "AsgTools/ToolHandle.h"
#include "AsgTools/AnaToolHandle.h"
#include "TruthClassification/TruthClassificationTool.h"

#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODJet/JetContainer.h"

#include <vector>

#include "PLIVTupleMaker/DecorateTracks.h"
#include "PLIVTupleMaker/DecorateMuons.h"
#include "PLIVTupleMaker/DecorateElectrons.h"
#include "PLIVTupleMaker/DecorateLeptons.h"

namespace top{
  class PLIVEventSaver : public top::EventSaverFlatNtuple {
    public:
      PLIVEventSaver();
      virtual ~PLIVEventSaver(){}
      
      virtual void initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches) override;
      virtual StatusCode initialize() override {return StatusCode::SUCCESS;}      
      virtual void calculateEvent(const top::Event& event) override;
      template<typename T> const xAOD::Jet* FindTrackJet(const T &part, const xAOD::JetContainer &jets);

    private:
      ToolHandle<CP::IClassificationTool> m_truthTool{"TruthClassificationTool", this};

      const float m_maxLepTrackJetDr{0.4};
      DecorateTracks m_TrackDecorater;
      DecorateMuons m_MuonDecorater;
      DecorateElectrons m_ElectronDecorater;
      DecorateLeptons m_LeptonDecorater;
      ClassDefOverride(top::PLIVEventSaver, 0);
  };
}

#endif
