#ifndef PLIVTUPLEMAKERLOADER_H
#define PLIVTUPLEMAKERLOADER_H

#include "TopEventSelectionTools/ToolLoaderBase.h"

namespace top{
  
  class PLIVTupleMakerLoader : public ToolLoaderBase {
    public:
      top::EventSelectorBase* initTool(const std::string& name, const std::string& line, TFile* outputFile, std::shared_ptr<top::TopConfig> config, EL::Worker* wk = nullptr);

      ClassDef(top::PLIVTupleMakerLoader, 0);
  };
}

#endif
