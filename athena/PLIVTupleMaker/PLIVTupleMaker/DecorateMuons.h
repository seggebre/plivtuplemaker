#ifndef DECORATEMUONS_H
#define DECORATEMUONS_H

#include "TopAnalysis/EventSaverFlatNtuple.h"
#include "TopConfiguration/ConfigurationSettings.h"
#include "TopEventSelectionTools/TreeManager.h"

#include "AsgTools/AsgTool.h"
#include "AsgTools/ToolHandle.h"
#include "AsgTools/AnaToolHandle.h"
#include "TruthClassification/TruthClassificationTool.h"

#include <AthContainers/AuxElement.h>
#include <AthLinks/ElementLink.h>

#include "PLIVTupleMaker/PLIVUtils.h"

namespace top{
  class DecorateMuons : public top::EventSaverFlatNtuple{
    public:
      DecorateMuons();
      virtual ~DecorateMuons() = default;

      void ProcessEvent(const top::Event& event, ToolHandle<CP::IClassificationTool>& iffTool);
      void SetBranches(std::shared_ptr<top::TreeManager> tree);
      const xAOD::TrackParticle* findTrackMuon(const xAOD::Muon *muon);
      bool checkTrackMuon(const xAOD::TrackParticle *trackLep);

    private:

      /*SG::ReadHandleKey<xAOD::MuonContainer> m_muonsInKey{
          this, "muonsIn", "", "containerName to read"};
      SG::ReadHandleKey<xAOD::JetContainer> m_trackjetsInKey{
          this, "trackjetsIn", "AntiKt4PV0Track", "containerName to read"};
      SG::ReadHandleKey<xAOD::EventInfo> m_eventInKey{
          this, "eventIn", "EventInfo", "containerName to read"};
      */
      std::vector<float> m_mu_calE;
      std::vector<float> m_mu_calERel;
      std::vector<float> m_mu_dRlj;
      std::vector<float> m_mu_dRljAll;
      std::vector<float> m_mu_EnergyLoss;
      std::vector<unsigned int> m_mu_iffClass;
      std::vector<float> m_mu_ptFrac;
      std::vector<float> m_mu_ptRaw;
      std::vector<float> m_mu_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500;
      std::vector<float> m_mu_ptvarcone30Rel;
      std::vector<float> m_mu_topoetcone30;
      std::vector<float> m_mu_topoetcone30Rel;
      std::vector<float> m_mu_topoetcone40;
      std::vector<float> m_mu_topoetcone40Rel;

      std::vector<int> m_mu_hasTrackMuon;
      std::vector<int> m_mu_hasTrackJet;
      std::vector<float> m_trackMu_pt;
      std::vector<float> m_trackMu_eta;
      std::vector<float> m_trackMu_phi;
      std::vector<float> m_trackMu_e;
      std::vector<int> m_trackMu_hasTrackJet;
      std::vector<float> m_trackMu_d0;
      std::vector<float> m_trackMu_d0Sig;
      std::vector<uint8_t> m_trackMu_nPixelHits;
      std::vector<uint8_t> m_trackMu_nPixelHoles;
      std::vector<uint8_t> m_trackMu_nPixelSharedHits;
      std::vector<uint8_t> m_trackMu_nSCTHits;
      std::vector<uint8_t> m_trackMu_nSCTHoles;
      std::vector<uint8_t> m_trackMu_nSCTSharedHits;
      std::vector<uint8_t> m_trackMu_nSiHits;
      std::vector<uint8_t> m_trackMu_nSiHoles;
      std::vector<float> m_trackMu_nSiSharedHits;
      std::vector<float> m_trackMu_z0;
      std::vector<float> m_trackMu_z0Sig;
      std::vector<float> m_trackMu_z0Sin;
      std::vector<float> m_trackMu_dRTrackJet;
      std::vector<float> m_trackMu_ptRelTrackJet;

      std::vector<float> m_trackMuTrackJet_pt;
      std::vector<float> m_trackMuTrackJet_eta;
      std::vector<float> m_trackMuTrackJet_phi;
      std::vector<float> m_trackMuTrackJet_e;

      const float m_maxLepTrackJetDr;
      const float m_maxDeltaZ0Sin;
      const float m_maxTopoEtCone30Rel;

  };
}

#endif
