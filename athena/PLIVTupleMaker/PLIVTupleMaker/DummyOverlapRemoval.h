#ifndef DUMMYOVERLAPREMOVAL_H
#define DUMMYOVERLAPREMOVAL_H

// system include(s):
#include <memory>
#include <vector>

#include "TopObjectSelectionTools/OverlapRemovalBase.h"

class DummyOverlapRemoval : public top::OverlapRemovalBase {
    public:
        DummyOverlapRemoval();
        virtual ~DummyOverlapRemoval();


        virtual void overlapremoval(const xAOD::PhotonContainer*   photon  ,
                                    const xAOD::ElectronContainer* el  ,
                                    const xAOD::MuonContainer*     mu  ,
                                    const xAOD::TauJetContainer*   tau ,
                                    const xAOD::JetContainer*      jet ,
                                    const xAOD::JetContainer*      ljet ,
                                    std::vector<unsigned int>& goodPhotons,
                                    std::vector<unsigned int>& goodElectrons,
                                    std::vector<unsigned int>& goodMuons,
                                    std::vector<unsigned int>& goodTaus,
                                    std::vector<unsigned int>& goodJets,
                                    std::vector<unsigned int>& goodLargeRJets,
                                    const bool isLooseEvent );


        virtual void print(std::ostream&) const;
};

#endif  

