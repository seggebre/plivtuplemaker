## Setup the Tuple Maker (for the first time)
If you set it up for the first time do:
```
mkdir build run
asetup AnalysisBase,24.2.37,here
```
The first time you compile the code you need to move to your `build/` folder and do
```
cmake ../athena/Projects/WorkDir/
cmake --build ./
source */setup.sh
```

### Setup the code (all other times)
All other times you only need to do
```
asetup --restore
source build/*/setup.sh
```


Config files can be found in the share folder of the PLIVTupleMaker.


## Comments
- The Overlap Removal Issue is fixed.
- secondary vertex information are missing
